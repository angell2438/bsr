<?php
/*
@author Dmitriy Kubarev
@link   http://www.simpleopencart.com
@link   http://www.opencart.com/index.php?route=extension/extension/info&extension_id=4811
 */

class ModelToolSimpleApiCustom extends Model {
	public function example($filterFieldValue) {
		$values = array();

		$values[] = array(
			'id' => 'my_id',
			'text' => 'my_text',
		);

		return $values;
	}

	public function checkCaptcha($value, $filter) {
		if (isset($this->session->data['captcha']) && $this->session->data['captcha'] != $value) {
			return false;
		}

		return true;
	}

	public function getYesNo($filter = '') {
		return array(
			array(
				'id' => '1',
				'text' => $this->language->get('text_yes'),
			),
			array(
				'id' => '0',
				'text' => $this->language->get('text_no'),
			),
		);
	}

	public function getCityd() {

		$name = $this->request->get['name'];

		$deliv = $this->request->get['deliv'];

		$sql = "SELECT * FROM `intime` WHERE `city_name_ua` like '%" . $name . "%' OR `city_name_ru` like '%" . $name . "%' limit 10 ";

		$return = $this->db->query($sql)->rows;

		foreach ($return as $key => $value) {
			# code...
			$res[] = ['city' => $value['city_name_ua'], 'area' => $value['area_name_ua'], 'city_id' => $value['id_city'], 'city_ru' => $value['city_name_ru']];
		}

		return (isset($res)) ? $res : '';
	}

	public function getDelivery($name = 'Хмельницький, Хмельницький') {

		if (!empty($this->request->post['shipping_method'])) {

			$this->session->data['deliv'] = $this->request->post['shipping_method'];

		}

		if (stristr($name, ',') === FALSE) {

			$returnn[0] = ['id' => 0, 'text' => 'Доставка не доступна'];

			return $returnn;

		}

		// soap::
		if (!empty($name)) {
			list($area, $city) = explode(',', $name);
		} else {
			$area = '';
			$city = '';
		}

		if (isset($this->session->data['deliv'])) {
			$deliv = $this->session->data['deliv'];

		} else {
			$deliv = 'intime.intime';

		}

		if ($deliv == 'delivery.delivery') {

			$deliv = 1;

		} else if ($deliv == 'intime.intime') {

			$deliv = 2;

		} else if ($deliv == 'nova.nova') {
			# code...
			$deliv = 3;
		}

		$soap = new Soap();
		$delivery = new Delivery();

		$sql = "SELECT * FROM `intime` WHERE `area_name_ua` like '%" . trim($area) . "%' and `city_name_ua` like '%" . trim($city) . "%'  limit 1 ";

		$return = $this->db->query($sql)->row;

		if ($deliv == 2) {

			$res_branch = $this->cache->get('intime.offices.' . (int) $return['id_city']);
			if ($res_branch == false) {

				$branch = $soap->getBranch($return['id_city']);
				$this->cache->set('intime.offices.' . (int) $return['id_city'], $branch);
				$res_branch = $this->cache->get('intime.offices.' . (int) $return['id_city']);

			}

			if (empty($res_branch)) {
				$returnn[0] = ['id' => 0, 'text' => 'Доставка не доступна'];
				return $returnn;

			}

			foreach ($res_branch as $key => $value) {
				# code...
				$returnn[] = ['id' => $value['id'], 'text' => $value['branch_name_ua'] . ', ' . $value['address_ua']];
			}

			if (empty($returnn[0])) {
				$returnn[0] = ['id' => 0, 'text' => 'Доставка не доступна'];

			}

		}

		if ($deliv == 1) {

			if (empty($return['id_delivery'])) {
				$returnn[0] = ['id' => 0, 'text' => 'Доставка не доступна'];

				return $returnn;

			}

			$res_branch = $this->cache->get('delivery.offices.' . (int) $return['id_delivery']);

			if ($res_branch == false) {
				$branch = $delivery->getBranch($return['id_delivery']);
				$this->cache->set('delivery.offices.' . (int) $return['id_delivery'], $branch);
				$res_branch = $this->cache->get('delivery.offices.' . (int) $return['id_delivery']);

			}

			foreach ($res_branch as $key => $value) {
				# code...

				$returnn[] = ['id' => $value['id'], 'text' => $value['name'] . ', ' . $value['address']];
			}

		}

		if ($deliv == 3) {

			$nova = new NovaPoshta();

			if (empty($return['id_nova'])) {
				$returnn[0] = ['id' => 0, 'text' => 'Доставка не доступна'];

				return $returnn;

			}

			$res_branch = $this->cache->get('delivery.nova.' . $return['id_nova']);

			if ($res_branch == false) {
				$branch = $nova->getBranchNova($return['id_nova']);
				$this->cache->set('delivery.nova.' . $return['id_nova'], $branch);
				$res_branch = $this->cache->get('delivery.nova.' . $return['id_nova']);

			}

			foreach ($res_branch as $key => $value) {
				# code...

                if ($this->session->data['language'] == 'ua') {
                    $returnn[] = ['id' => $value['Ref'], 'text' => $value['Description']];
                } else {
                    $returnn[] = ['id' => $value['Ref'], 'text' => $value['DescriptionRu']];
                }


			}

		}

		return !empty($returnn) ? $returnn : $returnn = ['0' => ['id' => 0, 'text' => 'Доставка не доступна']];

	}

}

class Soap {

	public $client;
	public $key = "15422991140001925840";

	function __construct() {

		$this->client = new SoapClient("http://esb.intime.ua:8080/services/intime_api_3.0?wsdl");

	}

	public function getContries() {

		return $this->client->GET_COUNTRY_BY_ID(['api_key' => $this->key, 'id' => '215']);

	}

	public function getArea() {

		return $this->client->GET_AREA_FILTERED(['api_key' => $this->key, 'country_id' => '215'])->Entry_get_area_filtered;

	}

	public function getRaione($id_area = '') {

		return $this->client->GET_DISTRICT_FILTERED(['api_key' => $this->key, 'area_id' => $id_area]);

	}

	public function getCity($id_raione = '', $name = '', $id_area = '') {

		return $this->client->get_locality_filtered(['api_key' => $this->key, 'district_id' => $id_raione, 'locality_name' => $name, 'area_id' => $id_area])->Entry_get_locality_filtered;
		//return $this->client->__getFunctions();

	}

	public function getBranch($id) {

		$branch = $this->client->GET_BRANCH_FILTERED(['api_key' => $this->key, 'locality_id' => $id]);

		if (isset($branch->Entry_get_branch_filtered)) {

			$res = $branch->Entry_get_branch_filtered;

		} else {
			$res = '';
		}

		return $res;

	}
}

/**
 *
 */
class Delivery {

	function __construct() {
		# code...
	}

	public function getCity($regionId, $culture = 'uk-UA', $contry = 1) {

		$parametr = 'culture=' . $culture . '&regionId=' . $regionId . '&country=' . $contry;

		return $this->curlGet('GetAreasList', htmlentities($parametr));

	}

	public function getBranch($CityId, $culture = 'uk-UA', $contry = 1) {
		$parametr = 'culture=' . $culture . '&CityId=' . $CityId . '&country=' . $contry;

		return $this->curlGet('GetWarehousesListInDetail', $parametr);

	}

	public function getArea($culture = 'uk-UA', $contry = 1) {
		$parametr = 'culture=' . $culture . '&country=' . $contry;

		return $this->curlGet('GetRegionList', htmlentities($parametr));

	}

	public function curlGet($method, $parametr, $access = 'Public') {

		$url_service = 'http://www.delivery-auto.com/api/v4/';
		$url = $url_service . $access . '/' . $method . '?' . $parametr;

		$curl = curl_init($url);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$curl_response = curl_exec($curl);
		if ($curl_response === false) {
			$info = curl_getinfo($curl);
			curl_close($curl);
			die('error occured during curl exec. Additioanl info: ' . var_export($info));
		}
		curl_close($curl);
		$decoded = json_decode($curl_response);
		if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
			die('error occured: ' . $decoded->response->errormessage);
		}
		return $decoded->data;

	}
}

/**
 *
 */
class NovaPoshta {

	public $api = '3008454fcc1f5929f81f1d7d17e4f920';

	function __construct() {
		# code...
	}

	public function curlGet($modelName, $calledMethod, $prop, $name = 'Рівне Рівненська', $access = 'Public') {

		$url_service = 'https://api.novaposhta.ua/v2.0/json/';
		$curl = curl_init($url_service);

		$data = 'apiKey=' . $this->api . '&modelName=' . $modelName . '&getWarehouses=' . $calledMethod;

		$arrayName = array('apiKey' => $this->api, 'modelName' => $modelName, 'calledMethod' => $calledMethod, 'methodProperties' => $prop);

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.novaposhta.ua/v2.0/json/",
			CURLOPT_RETURNTRANSFER => True,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($arrayName),
			CURLOPT_HTTPHEADER => array("content-type: application/json"),
		));

		$curl_response = curl_exec($curl);
		if ($curl_response === false) {
			$info = curl_getinfo($curl);
			curl_close($curl);
			die('error occured during curl exec. Additioanl info: ' . var_export($info));
		}
		curl_close($curl);
		$decoded = json_decode($curl_response);
		if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
			die('error occured: ' . $decoded->response->errormessage);
		}
		return $decoded->data;

	}

	public function getBranchNova($ref) {

		$prop = array('SettlementRef' => $ref);

		return $this->curlGet('AddressGeneral', 'getWarehouses', $prop);

	}

	public function getNameCity($page) {

		$prop = array('Page' => $page, 'Limit' => '100');

		return $this->curlGet('AddressGeneral', 'getSettlements', $prop);

	}
}
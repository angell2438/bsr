<?php
namespace deliv;

class Delivery
{
    public function __construct()
    {
        # code...
    }

    public function getCity($regionId, $culture = 'uk-UA', $contry = 1)
    {
        $parametr = 'culture=' . $culture . '&regionId=' . $regionId . '&country=' . $contry;

        return $this->curlGet('GetAreasList', htmlentities($parametr));
    }

    public function getBranch($CityId, $culture = 'uk-UA', $contry = 1)
    {
        $parametr = 'culture=' . $culture . '&CityId=' . $CityId . '&country=' . $contry;

        return $this->curlGet('GetWarehousesListInDetail', $parametr);
    }

    public function getArea($culture = 'uk-UA', $contry = 1)
    {
        $parametr = 'culture=' . $culture . '&country=' . $contry;

        return $this->curlGet('GetRegionList', htmlentities($parametr));
    }

    public function curlGet($method, $parametr, $access = 'Public')
    {
        $url_service = 'http://www.delivery-auto.com/api/v4/';
        $url = $url_service . $access . '/' . $method . '?' . $parametr;

        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            die('error occured: ' . $decoded->response->errormessage);
        }
        return $decoded->data;
    }
}

<?php

namespace deliv;

class Intime
{
    public $client;
    public $key = "15422991140001925840";

    public function __construct()
    {
        $this->client = new \SoapClient("http://esb.intime.ua:8080/services/intime_api_3.0?wsdl");
    }

    public function getContries()
    {
        return $this->client->GET_COUNTRY_BY_ID(['api_key' => $this->key, 'id' => '215']);
    }

    public function getArea()
    {
        return $this->client->GET_AREA_FILTERED(['api_key' => $this->key, 'country_id' => '215'])->Entry_get_area_filtered;
    }

    public function getRaione($id_area = '')
    {
        return $this->client->GET_DISTRICT_FILTERED(['api_key' => $this->key, 'area_id' => $id_area]);
    }

    public function getCity($id_raione = '', $name = '', $id_area = '')
    {
        return $this->client->get_locality_filtered(['api_key' => $this->key, 'district_id' => $id_raione, 'locality_name' => $name, 'area_id' => $id_area])->Entry_get_locality_filtered;
        //return $this->client->__getFunctions();
    }

    public function getBranch($id)
    {
        $branch = $this->client->GET_BRANCH_FILTERED(['api_key' => $this->key, 'locality_id' => $id]);

        if (isset($branch->Entry_get_branch_filtered)) {
            $res = $branch->Entry_get_branch_filtered;
        } else {
            $res = '';
        }

        return $res;
    }
}

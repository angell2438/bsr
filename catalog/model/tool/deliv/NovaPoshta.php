<?php
namespace deliv;

class NovaPoshta
{
    public $api = '3008454fcc1f5929f81f1d7d17e4f920';

    public function __construct()
    {
        # code...
    }

    public function curlGet($modelName, $calledMethod, $prop, $name = 'Рівне Рівненська', $access = 'Public')
    {
        $url_service = 'https://api.novaposhta.ua/v2.0/json/';
        $curl = curl_init($url_service);

        $data = 'apiKey=' . $this->api . '&modelName=' . $modelName . '&getWarehouses=' . $calledMethod;

        $arrayName = array('apiKey' => $this->api, 'modelName' => $modelName, 'calledMethod' => $calledMethod, 'methodProperties' => $prop);

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.novaposhta.ua/v2.0/json/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($arrayName),
            CURLOPT_HTTPHEADER => array("content-type: application/json"),
        ));

        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            die('error occured: ' . $decoded->response->errormessage);
        }
        return $decoded->data;
    }

    public function getBranchNova($ref)
    {
        $prop = array('SettlementRef' => $ref);

        return $this->curlGet('AddressGeneral', 'getWarehouses', $prop);
    }

    public function getNameCity($page)
    {
        $prop = array('Page' => $page, 'Limit' => '100');

        return $this->curlGet('AddressGeneral', 'getSettlements', $prop);
    }
}

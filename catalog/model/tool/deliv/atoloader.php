<?php

    spl_autoload_register(function ($class) {
        $class = str_replace('deliv\\', '/', $class);
        include __DIR__ . '/' . $class . '.php';
    });

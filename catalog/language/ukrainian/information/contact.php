<?php
// Heading
$_['heading_title']  = 'Контакти';

// Text
$_['text_location']  = 'Наша Адреса';
$_['text_store']     = 'Наші магазини';
$_['text_contact']   = 'Форма зв’язку';
$_['text_address']   = 'Адреса';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Час роботи';
$_['text_comment']   = 'Коментар';
$_['text_success']   = '<p>Ваш запит був успішно відправлений адміністрації магазину!</p>';
$_['btn_submit']     = 'Написати нам';

// Entry
$_['entry_name']     = 'Ім’я';
$_['entry_email']    = 'Електронна пошта';
$_['entry_enquiry']  = 'Повідомлення';

// Email
$_['email_subject']  = 'Повідомлення %s';

// Errors
$_['error_name']     = 'Ім’я має бути від 2 до 32 символів!';
$_['error_email']    = 'E-Mail вказано некоректно!';
$_['error_enquiry']  = 'Повідомлення має бути від 10 до 3000 символів!';

<?php
// Heading 
$_['heading_title']  = 'Зміна пароля';

// Text
$_['text_account']   = 'Особистий кабінет';
$_['text_success']   = 'Ваш пароль успішно змінений!';

// Entry
$_['entry_password'] = 'Новий пароль';
$_['entry_confirm']  = 'Повторіть новий пароль';
$_['entry_old_password']  = 'Поточний пароль';

// Error
$_['error_password'] = 'Пароль має бути від 6 до 20 символів!';
$_['error_old_password'] = 'Невірно введений поточний пароль';
$_['error_confirm']  = 'Паролі не співпадають!';
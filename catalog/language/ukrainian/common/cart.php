<?php
// Text
$_['text_items']     = 'товарів: %s <br> %s';
$_['text_items_count']   = '%s';
$_['text_empty']     = 'У кошику порожньо!';
$_['text_cart']      = 'Відкрити кошик';
$_['text_checkout']  = 'Оформити замовлення';
$_['text_recurring'] = 'Профіль платежу';
<?php
// Text
$_['text_information']  = 'Інформація';
$_['text_service']      = 'Служба підтримки';
$_['text_extra']        = 'Додатково';
$_['text_contact']      = 'Зворотній дзвінок';
$_['text_return']       = 'Повернення товару';
$_['text_sitemap']      = 'Мапа сайту';
$_['text_manufacturer'] = 'Виробники';
$_['text_voucher']      = 'Подарункові сертифікати';
$_['text_affiliate']    = 'Партнери';
$_['text_special']      = 'Товари зі знижкою';
$_['text_account']      = 'Особистий кабінет';
$_['text_order']        = 'Історія замовлень';
$_['text_wishlist']     = 'Мої закладки';
$_['text_newsletter']   = 'Розсилка новин';
$_['text_category']   = 'Категорії';
$_['text_powered']      = 'Copyright © 2017. Сяйво БСР. Всі права захищені.';

$_['entry_name']      	= 'Ім\'я';
$_['entry_phone']       = 'Номер мобільного телефону';
$_['text_call']      	= 'Замовити дзвінок';
$_['text_send']      	= 'Відправити';
$_['text_loading']      = 'Опрацювання';
<?php
// Text
$_['text_signin_register']    = 'Увійдіть або зареєструйтеся';
$_['text_login']   			  = 'Вхід';
$_['text_register']    		  = 'Зареєструватись';

$_['text_new_customer']       = 'Реєстрація';
$_['text_returning']          = 'Вхід';
$_['text_social_login']       = 'Вхід через соцмережі';
$_['text_returning_customer'] = 'I am a returning customer';
$_['text_details']            = 'Your Personal Details';
$_['entry_email']             = 'Електронна пошта ';
$_['entry_name']              = 'Ім\'я';
$_['entry_password']          = 'Пароль';
$_['entry_lastname']          = 'Фамілія';
$_['entry_repassword']        = 'Повторіть пароль';
$_['entry_telephone']         = 'Телефон';
$_['text_forgotten']          = 'Забули пароль?';
$_['text_show_pass']          = 'Показати пароль';
$_['text_agree']              = 'I agree to the <a href="%s" class="agree"><b>%s</b></a>';



//Button
$_['button_login']            = 'Вхід';

//Error
$_['error_name']           = 'Ім\'я повинно бути від 2 до 32 символів!';
$_['error_email']          = 'E-Mail невірний!';
$_['error_lastname']       = 'Фамілія має містити від 2 до 32 символів!';
$_['error_telephone']      = 'Телефон повинен бути від 10 символів!';
$_['error_password']       = 'Пароль повинен бути від 4 до 20 символів!';
$_['error_repassword']     = 'Паролі не співпадають!';
$_['error_exists']         = 'E-Mail вже зареєстрований!';
$_['error_agree']          = 'Вы должны согласиться с %s!';
$_['error_warning']        = 'Будь ласка, уважно перевірте форму на наявність помилок!';
$_['error_approved']       = 'Ваша учетная запись требует одобрения, прежде чем Вы можете войти в систему.';
$_['error_login']          = 'E-Mail не зареєстрований.';
<?php
// Heading 
$_['heading_title']  = 'Смена пароля';

// Text
$_['text_account']   = 'Личный кабинет';
$_['text_success']   = 'Ваш пароль успешно изменен!';

// Entry
$_['entry_password'] = 'Новый пароль';
$_['entry_confirm']  = 'Повторите новый пароль';
$_['entry_old_password']  = 'Текущий пароль';

// Error
$_['error_password'] = 'Пароль должен быть от 6 до 20 символов!';
$_['error_old_password'] = 'Неверно введен текущий пароль';
$_['error_confirm']  = 'Пароли не совпадают!';
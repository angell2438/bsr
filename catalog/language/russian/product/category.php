<?php
// Text
$_['text_refine']       = 'Уточнить поиск';
$_['text_product']      = 'Товары';
$_['text_error']        = 'Категория не найдена!';
$_['text_empty']        = 'В этой категории нет товаров.';
$_['text_quantity']     = 'Кол-во:';
$_['text_manufacturer'] = 'Производитель:';
$_['text_model']        = 'Код товара:';
$_['text_points']       = 'Бонусные Баллы:';
$_['text_price']        = 'Цена:';
$_['text_tax']          = 'Без налога:';
$_['text_compare']      = 'Сравнение товаров (%s)';
$_['text_sort']         = 'Сортировать по:';
$_['text_default']      = 'умолчанию';
$_['text_name_asc']     = 'Имени (A - Я)';
$_['text_name_desc']    = 'Имени (Я - A)';
$_['text_price_asc']    = 'Цене (возрастанию)';
$_['text_price_desc']   = 'Цене (убыванию)';
$_['text_rating_asc']   = 'Рейтингу (возрастанию)';
$_['text_rating_desc']  = 'Рейтингу (убыванию)';
$_['text_model_asc']    = 'Модели (A - Я)';
$_['text_model_desc']   = 'Модели (Я - A)';
$_['text_limit']        = 'Показывать по:';

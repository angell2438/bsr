<?php
// Text
$_['text_signin_register']    = 'Войдите или зарегистрируйтесь';
$_['text_login']   			  = 'Войти';
$_['text_register']    		  = 'Зарегистрироваться';

$_['text_new_customer']       = 'Регистрация';
$_['text_returning']          = 'Войти';
$_['text_social_login']       = 'Войти через соц. сети';
$_['text_returning_customer'] = 'I am a returning customer';
$_['text_details']            = 'Your Personal Details';
$_['entry_email']             = 'Електронная почта';
$_['entry_name']              = 'Имя';
$_['entry_password']          = 'Пароль';
$_['entry_lastname']          = 'Фамилия';
$_['entry_repassword']        = 'Повторите пароль';
$_['entry_telephone']         = 'Телефон';
$_['text_forgotten']          = 'Забыли пароль?';
$_['text_show_pass']          = 'Показать пароль';
$_['text_agree']              = 'I agree to the <a href="%s" class="agree"><b>%s</b></a>';



//Button
$_['button_login']            = 'Войти';

//Error
$_['error_name']           = 'Имя должно быть от 2 до 32 символов!';
$_['error_lastname']       = 'Фамілія должна быть от 2 до 32 символов!';
$_['error_email']          = 'E-Mail не действителен!';
$_['error_telephone']      = 'Телефон должен находиться в пределах от 10 символов!';
$_['error_password']       = 'Пароль должен быть от 4 до 20 символов!';
$_['error_repassword']     = 'Пароли не совпадают!';
$_['error_exists']         = 'E-Mail уже зарегистрирован!';
$_['error_agree']          = 'Вы должны согласиться с %s!';
$_['error_warning']        = 'Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_approved']       = 'Ваша учетная запись требует одобрения, прежде чем Вы можете войти в систему.';
$_['error_login']          = 'E-Mail не зарегистрирован!';
<?php

require_once 'library/curlApi.php';
require_once 'library/twiter.php';
require_once 'library/const.php';

class ControllerAuthOauth extends Controller {

    protected $token;

    protected $arraySetting = [];

    function __construct($param) {

        $this->arraySetting = [
            'twiter' => [
                'secret' => 'cOHa4xwAZyA9uRQcpgM7C1wDgAIZelKmbRQ02SuyNT1vvYOfTf',
                'nonce' => 'jD2T1wOoSoMLxw8bdUsPJ0V0W',
                'calback' => 'http://ocstore.dix.ua/tlogin',
            ],
            'facebook' => [
                'app' => '1926182117646388',
            ],
            'google' => [
                'id' => 'cOHa4xwAZyA9uRQcpgM7C1wDgAIZelKmbRQ02SuyNT1vvYOfTf',
                'secret' => 'jD2T1wOoSoMLxw8bdUsPJ0V0W',
                'calback' => 'http://ocstore.dix.ua/glogin',
            ],
        ];

        return parent::__construct($param);

    }

    //block twiter

    protected function getTwUser($oauth_token, $oauth_token_secret, $user_id) {

        list($key_token, $token) = explode('=', $oauth_token);
        list($key_secret, $secret) = explode('=', $oauth_token_secret);
        list($key_id, $id) = explode('=', $user_id);
        ////////////////////////////////////////////////

        $twiterSetting = $this->arraySetting['twiter'];
        $href_api = 'https://api.twitter.com/1.1/users/lookup.json';

        $twiter = new Twiter($href_api, $twiterSetting['nonce']);
        $twiter->setCustomHeader(['oauth_token' => $token]);
        $twiter->setSignature($twiterSetting['secret'], $secret, 'GET', $id);

        $header = $twiter->getHeaderOauth();
        $header[] = 'Content-Type: application/json';
        $header[] = 'Accept: application/json';

        $curl = new curlApi($href_api . '?user_id=' . $id, $header);
        $curl->curlGet();
        $twiter_responce = $curl->CurlExe(1);

        $data = [
            'id' => $twiter_responce[0]->id,
            'firstname' => $twiter_responce[0]->name,
            'lastname' => '',
            'email' => '',
        ];
        return $data;
    }

    public function tlogin() {

        $twiterSetting = $this->arraySetting['twiter'];
        $href_api = 'https://api.twitter.com/oauth/access_token';

        $twiter = new Twiter($href_api, $twiterSetting['nonce']);

        if (isset($this->request->get['oauth_token']) && isset($this->request->get['oauth_verifier'])) {

            $twiter->setCustomHeader(['oauth_token' => $this->request->get['oauth_token']]);
            $twiter->setSignature($twiterSetting['secret'], $this->request->get['oauth_token']);

            $header = $twiter->getHeaderOauth();

            $curl = new curlApi($href_api, $header);
            $curl->CurlPost(['oauth_verifier' => $this->request->get['oauth_verifier']]);

            $twiter_responce = $curl->CurlExe();
            /////
            ///////////////
            unset($twiter);
            ///////////////

            list($oauth_token, $oauth_token_secret, $user_id, $screen_name, $x_auth_expires) = explode('&', $twiter_responce);

            $this->getDataOauth(3, [
                'oauth_token' => $oauth_token,
                'oauth_token_secret' => $oauth_token_secret,
                'user_id' => $user_id,
            ]);
        }

    }

    public function tlogin_request() {

        $twiterSetting = $this->arraySetting['twiter'];

        $href_api = 'https://api.twitter.com/oauth/request_token';

        $twiter = new Twiter($href_api, $twiterSetting['nonce']);
        $twiter->setCustomHeader(['oauth_callback' => $twiterSetting['calback']]);
        $twiter->setSignature($twiterSetting['secret']);

        $header = $twiter->getHeaderOauth();

        $curl = new curlApi($href_api, $header);
        $curl->CurlPost();

        $twiter_responce = $curl->CurlExe(0);

        list($oauth_token, $oauth_token_secret, $oauth_callback_confirmed) = explode('&', $twiter_responce);
        list($key, $token) = explode('=', $oauth_token);
        $this->response->redirect('https://api.twitter.com/oauth/authenticate?oauth_token=' . $token);

    }
    //////////////////////////////////////////////////////////////////////

    public function windowGlogin() {
        $this->response->setOutput($this->load->view('default/template/oauth/oauth.tpl'));

    }

    public function glogin() {
        //Include configuration
        include __DIR__ . '/config.php';

        //Include js script
        $this->document->addScript('catalog/view/javascript/oauth/oauth.js');
        $code = $this->request->get['code'];

        // Get configuration google
        $redirect = $this->arraySetting['google']['callback'];
        $client_id = $this->arraySetting['google']['id'];
        $client_secret = $this->arraySetting['google']['secret'];

        //Створюємо редирект гоогла
        $redirect2 = 'https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&access_type=offline&include_granted_scopes=true&state=state_parameter_passthrough_value&' .
            'redirect_uri=' . $redirect . '&response_type=code&client_id=' . $client_id . '';

        if (!isset($code)) {
            $this->response->redirect($redirect2);
        } else {

            $ch = curl_init();
            // GET запрос указывается в строке URL

            curl_setopt($ch, CURLOPT_URL, 'https://accounts.google.com/o/oauth2/token');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/x-www-form-urlencoded',
            ));
            curl_setopt($ch, CURLOPT_POST, 1); //передача данных методом POST
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //теперь curl вернет нам ответ, а не выведет
            curl_setopt($ch, CURLOPT_POSTFIELDS, //тут переменные которые будут переданы методом POST
                http_build_query(array(
                    'code' => $code,
                    'client_id' => $client_id,
                    'client_secret' => $client_secret,
                    'redirect_uri' => $redirect, //это на случай если на сайте, к которому обращаемся проверяется была ли нажата кнопка submit, а не была ли оправлена форма
                    'grant_type' => 'authorization_code',
                )));
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 40);
            curl_setopt($ch, CURLOPT_USERAGENT, 'PHP Bot');
            $data = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($data);
            $this->getDataOauth(2,$data->access_token);

        }
    }

    public function getDataOauthFb() {
        $this->getDataOauth($this->request->post['social'],$this->request->post['access']);
    }

    public function getDataOauth($social, $access) {

        $this->load->model('oauth/oauth');
        if ($social == FACEBOOK) {
            //facebook
            $href_api = 'https://graph.facebook.com'
                . '/me?fields=email,about,first_name,last_name&access_token='
                . $access;

            $header[] = 'Content-Type: application/json';
            $header[] = 'Accept: application/json';

            $curl = new curlApi($href_api, $header);
            $curl->curlGet();
            $responce = json_decode($curl->CurlExe());
            $data = [
                'firstname' => $responce->first_name,
                'lastname' => $responce->last_name,
                'email' => $responce->email,
                'id' => $responce->id,
                'social' => $social,
            ];

        } else if ($social == GOOGLE) {
            //google

            $href_api = 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' . $access;

            $header[] = 'Content-Type: application/json';
            $header[] = 'Accept: application/json';

            $curl = new curlApi($href_api, $header);
            $curl->curlGet();
            $responce = json_decode($curl->CurlExe());

            $data = [
                'firstname' => $responce->given_name,
                'lastname' => $responce->family_name,
                'email' => $responce->email,
                'id' => $responce->id,
                'social' => $social,
            ];

        } else if ($social == TWITTER) {
            //twiter

            $data = $this->getTwUser(
                $access['oauth_token'],
                $access['oauth_token_secret'],
                $access['user_id']
            );

            $data['social'] = $social;

            $email = $this->model_oauth_oauth->getUserOauthIdSocial($data['id'], $data['social']);

            if (!empty($email)) {
                $data['email'] = $email;

            }

        }

        $this->session->data['password_token_auth'] = $data['password']
            =
            $this->model_oauth_oauth->getTokenGenerate($data['id'], $social);
        $this->model_oauth_oauth->setToken($data['password'], $social, $data['id']);

        $this->session->data['password_token_auth'] = $data['password'];

        if (isset($data['id'])) {

            if ($this->model_oauth_oauth->validateIdUserOauth($data['id'], $data['social'])
                &&
                $this->model_oauth_oauth->validateEmailUser($data['email'])) {

                $this->signUpUser($data);

                if ($social == 3) {

                    $this->response->redirect($this->url->link('auth/oauth/successSignUp', '', 'SSL'));

                }
            } else {

                $this->setUserNew($data);

                if ($social == 3) {

                    $this->response->redirect($this->url->link('auth/oauth/successSignUp', '', 'SSL'));

                }

            }
            if (!empty($google_access)) {
                $this->response->setOutput('<script>window.close()</script>');
            }

        } else {

            return true;
        }

    }

    public function signUpUser($user) {

        $this->load->model('account/customer');

        if (!$this->customer->login($user['email'], "", true)) {

            $this->model_account_customer->addLoginAttempt($user['email']);
        } else {
            $this->load->model('oauth/oauth');

            $this->model_account_customer->deleteLoginAttempts($user['email']);

            //$this->model_oauth_oauth->setToken($user['password'], $this->customer->getId());

        }
        return true;

    }

    public function successSignUp() {

        $this->load->model('account/customer');

        $this->load->model('oauth/oauth');

        $res = $this->model_oauth_oauth->getOauth($this->session->data['password_token_auth']);

        if (empty($res['email'])) {
            $this->response->redirect($this->url->link('auth/field/email_request', '', 'SSL'));
        }

        if ($res['status'] == 0) {
            $this->response->redirect($this->url->link('auth/field', '', 'SSL'));
        } else {
            unset($this->session->data['password_token_auth']);
            $this->response->redirect($this->url->link('account/account', '', 'SSL'));
        }
    }

    public function continueSignUp($user = '') {
        $this->load->model('account/customer');
        $this->load->model('oauth/oauth');

        if (empty($user)) {
            $user = $this->model_oauth_oauth->getOauth($this->session->data['password_token_auth']);
            $user['password'] = $this->session->data['password_token_auth'];
            $user['id'] = $user['id_auth'];

        }

        if (isset($user['telephone'])) {
            $user['telephone'] = $user['telephone'];
        } else {
            $user['telephone'] = '';
        }

        if (!empty($user['firstname'])) {
            $user['firstname'] = $user['firstname'];
        } else {
            $user['firstname'] = $user['name'];
        }

        if (isset($user['lastname'])) {
            $user['lastname'] = $user['lastname'];
        } else {
            $user['lastname'] = '';
        }

        if (isset($user['fax'])) {
            $user['fax'] = $user['fax'];
        } else {
            $user['fax'] = '';
        }

        if (isset($user['company'])) {
            $user['company'] = $user['company'];
        } else {
            $user['company'] = '';
        }

        if (isset($user['website'])) {
            $user['website'] = $user['website'];
        } else {
            $user['website'] = '';
        }

        if (isset($user['address_1'])) {
            $user['address_1'] = $user['address_1'];
        } else {
            $user['address_1'] = '';
        }

        if (isset($user['address_2'])) {
            $user['address_2'] = $user['address_2'];
        } else {
            $user['address_2'] = '';
        }

        if (isset($user['postcode'])) {
            $user['postcode'] = $user['postcode'];
        } else {
            $user['postcode'] = '';
        }

        if (isset($user['city'])) {
            $user['city'] = $user['city'];
        } else {
            $user['city'] = '';
        }

        if (isset($user['country_id'])) {
            $user['country_id'] = (int) $user['country_id'];
        } else {
            $user['country_id'] = $this->config->get('config_country_id');
        }

        if (isset($user['zone_id'])) {
            $user['zone_id'] = (int) $user['zone_id'];
        } else {
            $user['zone_id'] = '';
        }

        $res = $this->model_oauth_oauth->validateEmailUser($user['email']);
        if ($res === false) {
            $customer_id = $this->model_account_customer->addCustomer($user);
        } else {

            if (!$this->customer->login($user['email'], "", true)) {

                $this->model_account_customer->addLoginAttempt($user['email']);

            } else {

                $this->model_account_customer->deleteLoginAttempts($user['email']);

            }
            $customer_id = $res;
        }
        $user['id_c'] = $customer_id;

        //Чистим попередню привязь якщо вона Є
        //$this->model_oauth_oauth->deleteOauthUser($user['id']);

        $this->model_oauth_oauth->updateOautUser($user['id_c'], $user['password']);
        // Clear any previous login attempts for unregistered accounts.
        $this->model_account_customer->deleteLoginAttempts($user['email']);

        $this->customer->login($user['email'], $user['password']);

        unset($this->session->data['guest']);

        // Add to activity log
        $this->load->model('account/activity');

        $activity_data = array(
            'customer_id' => $customer_id,
            'name' => $user['firstname'] . ' ' . $user['lastname'],
        );

        $this->model_account_activity->addActivity('register', $activity_data);

        if ($user['social'] == 3) {

            $this->response->redirect($this->url->link('auth/oauth/successSignUp', '', 'SSL'));

        }

        return true;

    }

    public function setUserNew($user) {

        $this->load->model('account/customer');
        //Регистрация

        $this->model_oauth_oauth->createTemporalyNewOautUser($user);

        if (!empty($user['email'])) {
            $this->continueSignUp($user);
        }

    }

    public function config_fb() {

        $id_app_facebook = $this->arraySetting['facebook']['app'];
        $this->response->setOutput(json_encode(["facebook" => $id_app_facebook]));

    }

    public function error() {

        $data['heading_title'] = '';

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/oauth/error.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/oauth/error.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/oauth/eroor.tpl', $data));
        }
    }

}
<?php

class Twiter {

	public $href;
	public $consumerKey = 'jD2T1wOoSoMLxw8bdUsPJ0V0W';
	protected $signatureMethod = 'HMAC-SHA1';

	public $setTing;

	public $arrayOauth;

	function __construct($href, $consumerKey, $calback = '') {

		$oauth = [
			'oauth_consumer_key' => $consumerKey,
			'oauth_nonce' => (string) mt_rand(),
			'oauth_signature_method' => 'HMAC-SHA1',
			'oauth_timestamp' => time(),
			'oauth_version' => '1.0',
		];

		$this->arrayOauth = array_map("urlencode", $oauth);

		$this->href = $href;
	}

	public function setCustomHeader($header = []) {

		foreach ($header as $key => $headerValue) {
			# code...
			$this->arrayOauth[$key] = rawurlencode($headerValue);
		}

	}

	public function setSignature($secret, $token = '', $method = 'POST', $id = NULL) {

		$key = rawurlencode($secret) . "&" . rawurlencode($token);

		asort($this->arrayOauth); // secondary sort (value)
		ksort($this->arrayOauth);

		if (!empty($id)) {

			$this->arrayOauth['user_id'] = $id;

		};

		$querystring = urldecode(http_build_query($this->arrayOauth, '', '&'));
		$final_for_signature = $method . "&" . rawurlencode($this->href) . "&" . rawurlencode($querystring);
		$signature = rawurlencode(base64_encode(hash_hmac('sha1', $final_for_signature, $key, true)));

		$this->arrayOauth['oauth_signature'] = $signature;

	}

	public function getHeaderOauth() {
		ksort($this->arrayOauth);

		$auth = "OAuth " . urldecode(http_build_query($this->arrayOauth, '', ', '));
		$header = array("Authorization: $auth");
		return $header;

	}

}
<?php
class curlApi {

	public $curl;

	function __construct($href_api, $header = []) {

		//curl init
		$this->curl = curl_init();

		//curl setopt
		curl_setopt($this->curl, CURLOPT_URL, $href_api);
		curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($this->curl, CURLINFO_HEADER_OUT, true);
		/////////////////////////////////////////////////////

	}

	public function curlGet() {

		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);

	}

	public function CurlPost($param = []) {

		curl_setopt($this->curl, CURLOPT_HEADER, false); //передача данных методом POST
		curl_setopt($this->curl, CURLOPT_POST, 1); //передача данных методом POST
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1); //теперь curl вернет нам ответ, а не выведет
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $param);

	}

	public function CurlExe($json = 0) {
		//curl exec
		curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, 40);
		curl_setopt($this->curl, CURLOPT_USERAGENT, 'PHP Bot');

		$data = curl_exec($this->curl);

		if ($json == 0) {
			$result = $data;

		} else {
			$result = json_decode($data);
		}
		//curl close
		curl_close($this->curl);
		//decode in json format
		return $result;
	}

}

?>
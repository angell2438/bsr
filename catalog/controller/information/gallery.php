<?php
class ControllerInformationGallery extends Controller {
	public function index() {
		$this->load->language('information/gallery');

		$this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js');
		$this->document->addStyle('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);


        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $limit = 6;
//        $limit = $this->config->get('config_limit');

		if($this->config->get('gallery_status')){

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('information/gallery')
            );
            $data['heading_title'] = $this->language->get('heading_title');
            $data['text_empty'] = $this->language->get('text_error');

            $this->load->model('design/banner');
            $this->load->model('tool/image');
            $this->load->model('tool/imagen');

            $data['banners'] = array();

            $filter_data = array(
                'banner_id' => $this->config->get('gallery_id'),
                'start'              => ($page - 1) * $limit,
                'limit'              => $limit
            );

            $image_total = $this->model_design_banner->getTotalImages($filter_data);

            $results = $this->model_design_banner->getBannerImages($filter_data);

            foreach ($results as $result) {
                if (is_file(DIR_IMAGE . $result['image'])) {
                    $data['banners'][] = array(
                        'title' => $result['title'],
                        'link'  => $result['link'],
                        'image' => $this->model_tool_imagen->cropsize($result['image'], 360, 240),
                        'popup' => 'image/'.$result['image']
                    );
                }
            }

            $pagination = new Pagination();
            $pagination->total = $image_total;
            $pagination->page = $page;
            $pagination->limit = $limit;

            $pagination->text_prev = '<i class="icon-left-arrow" aria-hidden="true"></i>';
            $pagination->text_next = '<i class="icon-right-arrow" aria-hidden="true"></i>';

            $pagination->url = $this->url->link('information/gallery', 'page={page}');


            $data['pagination'] = $pagination->render();

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/gallery.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/gallery.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/information/gallery.tpl', $data));
            }
        }else{


            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('information/gallery')
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
            }
        }
	}

	public function agree() {
		$this->load->model('catalog/information');

		if (isset($this->request->get['information_id'])) {
			$information_id = (int)$this->request->get['information_id'];
		} else {
			$information_id = 0;
		}

		$output = '';

		$information_info = $this->model_catalog_information->getInformation($information_id);

		if ($information_info) {
			$output .= html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8') . "\n";
		}

		$this->response->setOutput($output);
	}
}
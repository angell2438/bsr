<?php
class ControllerInformationDownload extends Controller {
	public function index() {
		$this->load->language('information/download');

		$this->load->model('account/download');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

        $this->document->setTitle($this->language->get('heading_title'));

        $data['empty_text'] = $this->language->get('empty_text');
        $data['download_text'] = $this->language->get('download_text');

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/download', '', 'SSL')
        );

        $data['files'] = array();
        $results = $this->model_account_download->getDownloadsFile();
        if($results){
            foreach ($results as $result){
                if (file_exists(DIR_DOWNLOAD . $result['filename'])) {
                    $data['files'][] = array(
                        'name' => $result['name'],
                        'image' => 'image/' . $result['image'],
                        'file' => $this->url->link('information/download/download', 'download_id=' . $result['download_id'], 'SSL')
                    );
                }
            }
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['continue'] = $this->url->link('common/home');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/download.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/download.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/information/download.tpl', $data));
        }
	}
    public function download() {

        $this->load->model('account/download');

        if (isset($this->request->get['download_id'])) {
            $download_id = $this->request->get['download_id'];
        } else {
            $download_id = 0;
        }

        $download_info = $this->model_account_download->getDownloadFile($download_id);

        if ($download_info) {
            $file = DIR_DOWNLOAD . $download_info['filename'];
            $mask = basename($download_info['mask']);

            if (!headers_sent()) {
                if (file_exists($file)) {
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="' . ($mask ? $mask : basename($file)) . '"');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));

                    if (ob_get_level()) {
                        ob_end_clean();
                    }

                    readfile($file, 'rb');

                    exit();
                } else {
                    exit('Error: Could not find file ' . $file . '!');
                }
            } else {
                exit('Error: Headers already sent out!');
            }
        } else {
            $this->response->redirect($this->url->link('information/download', '', 'SSL'));
        }
    }
}
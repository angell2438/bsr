$(window).on("load resize", function() {
    var container =  $('.mini-menu');
    if ($(window).width() <= 992) {

        $('#search').appendTo('.sm-search');
        $('.mini-menu').appendTo('.container-for-menu');
        $('.category-block').appendTo('.container-catalog_menu');
    } else{
        $('#search').appendTo('.md-search');
        $('.mini-menu').appendTo('.ex1-collapse-con');
        $('.category-block').appendTo('#column-left');
    }
    // if ($(window).width() <= 767) {
    //     $('.wrapper-bascat').insertAfter('.float-left');
    //     $('.col-apteka-product').toggleClass('mini-apteka-items');
    //     $('.col-apteka-info').on('click', function () {
    //         $(this).parent().parent().toggleClass('open-aptek');
    //     });
    //     $('.wish-btn').removeAttr("data-toggle");
    //     $('.wish-btn').removeAttr("data-original-title");
    //     $('.wish-btn').removeAttr("title");
    //     $('.product-thumb .image p').dotdotdot({
    //         height: 30
    //     });
    // } else{
    //     $('.wrapper-bascat').insertAfter('.js-position-bascat');
    //     $('.col-apteka-product').removeClass('mini-apteka-items');
    //
    // }
});
$(document).ready(function () {

    if ($(window).width() <= 992) {
        $('#search').appendTo('.sm-search');
        $('.mini-menu').appendTo('.container-for-menu');
        $('.category-block').appendTo('.container-catalog_menu');
    } else{
        $('#search').appendTo('.md-search');
        $('.mini-menu').appendTo('.ex1-collapse-con');
        $('.category-block').appendTo('#column-left');
    }
    $('.btn-toggle-menu').on('click', function () {
        $(this).toggleClass('active-btn');
        $('body').toggleClass('no-scroll');
        $('.container-for-menu').toggleClass('active-menu');
    });

    $('.btn-toggle-catalog').on('click', function () {
        $(this).toggleClass('active-btn');
        $('body').toggleClass('no-scroll');
        $('.container-catalog_menu').toggleClass('active-menu');
    });

    //slider
    $('.wrapper_big-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        infinite: true,
        dots: true,
        initialSlide: 0
    });

    $('.wrapper_prod-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        infinite: true,
        dots: false,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-left-arrow" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-arrow" aria-hidden="true"></i></button>',
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 990,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 720,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 520,
                settings: {
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }

            }
        ]
    });
    //matchHeight

    $('.product-thumb').matchHeight({
        property: 'height'
    });

    $('.file_flex').matchHeight();

    // $('.collapse').collapse({
    //     // toggle: false
    // });

    $('input[type=tel], input[name="phone"], #customer_telephone, input[name="telephone"]').mask('(999) 999-99-99');

    $('select.form-control').select2({
        minimumResultsForSearch: Infinity
    });

    //cart-left
    // $('.btn-left-cart').click(function () {
    //     if ($('.wrapper-cart').hasClass('open')){
    //         $('.wrapper-cart').parent().addClass('open-parent');
    //     } else{
    //         $('.wrapper-cart').parent().removeClass('open-parent');
    //     }
    // });



    //focus-fir-input

    $('input, textarea').focus(function () {
        $(this).closest('.has-error').find('.simplecheckout-error-text').remove();
        $(this).closest('.has-error').find('.text-danger').remove();
        $(this).closest('.has-error').removeClass('has-error');
    });

    $('input, textarea').focus(function () {
        $(this).closest('form').find('.alert-danger').remove();
    });

    $('.form-style input').focus(function() {
        $(this).closest('.form-group ').find('.simplecheckout-rule-group').remove();
        $(this).closest('.form-group ').removeClass('has-error');
        $(this).closest('.form-group ').find('.error').remove();
        $(this).closest('.form-group ').find('.text-danger').remove();
    });
    $('.form-group input').focus(function() {
        $(this).closest('.form-group ').find('.simplecheckout-rule-group').remove();
        $(this).closest('.form-group ').removeClass('has-error ');
        $(this).closest('.form-group ').find('.error').remove();
        $(this).closest('.form-group ').find('.text-danger').remove();
    });

    $('.form-group textarea').focus(function() {
        $(this).closest('.form-group ').find('.simplecheckout-rule-group').remove();
        $(this).closest('.form-group ').removeClass('has-error ');
        $(this).closest('.form-group ').find('.error').remove();
        $(this).closest('.form-group ').find('.text-danger').remove();
    });

    $(document).on('click', '.btn-simple button', function () {
        $('#'+$(this).data('ids')).trigger('click');
    });

    $('.show_pass_ch input').change(function () {
        if($(this).is(':checked')){
            $(this).parent().parent().parent().parent().find('input[name=password]').attr('type','text');
        }else{
            $(this).parent().parent().parent().parent().find('input[name=password]').attr('type','password');

        }
    });

});
<div class="category-block" role="tablist" aria-multiselectable="true">
    <h2>Каталог товарів</h2>
    <div class="accordeon">
        <?php foreach ($categories as $category) { ?>
            <?php if ($category['children']) { ?>
                <?php
                foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) {
                     ?>
                    <?php $k = ""; foreach ($children as $child) { $k++;?>
                        <?php if ($child['grand_childs']) { ?>
                            <div class="panel-heading" role="tab" id="heading<?php echo $k; ?>">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $k; ?>"
                                   aria-expanded="true" aria-controls="collapseOne" class="collapsed">
                                    <?php echo $child['name']; ?>
                                    <span class="icon-up-arrow"></span>
                                </a>
                            </h4>
                            <div id="collapse<?php echo $k; ?>" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="heading<?php echo $k; ?>">
                                <ul class="collapse-list list-unstyled clearfix">
                                    <?php foreach ($child['grand_childs'] as $grand_child) { ?>
                                        <li>
                                            <a href="<?php echo $grand_child['href']; ?>">
                                               <?php echo $grand_child['name']; ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>

                        <?php } else { ?>
                            <div class="panel-heading"  id="heading<?php echo $k; ?>">
                                <h4 class="panel-title">
                                    <a href="<?php echo $child['href']; ?>">
                                        <?php echo $child['name']; ?>
                                    </a>
                                </h4>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>
</div>
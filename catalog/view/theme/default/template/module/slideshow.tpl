<div id="slideshow<?php echo $module; ?>" class="wrapper_big-slider">
  <?php foreach ($banners as $banner) { ?>
  <div class="item">
    <?php if ($banner['link']) { ?>
        <a href="<?php echo $banner['link']; ?>">
            <img src="<?php echo $banner['image']; ?>" alt="banner_bsr" class="img-responsive" />
            <span class="container-banner_title">
                <?php echo $banner['title']; ?>
            </span>

        </a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="banner_bsr" class="img-responsive" />
        <span class="container-banner_title">
                <?php echo $banner['title']; ?>
            </span>
    <?php } ?>
  </div>
  <?php } ?>
</div>
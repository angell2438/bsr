<div class="subscribe_wrapper">

    <div class="container">
            <form class="flex-row" action="" method="post">
            <div class="col-md-3 col-sm-12">
                <div class="row">
                    <h2><?php echo $heading_title; ?></h2>
                </div>
            </div>
            <div class="col-md-6 col-sm-8">
                <div class="row">
                    <div class="form-group required">
                        <input type="email" name="txtemail" id="txtemail" value="" placeholder="<?php echo $text_input; ?>" class="form-control input-lg"  />
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4">
                <div class="row">
                    <div class="form-group required clearfix">
                        <button type="submit" class="btn btn-pink" onclick="return subscribe();"><?php echo $btn_text; ?></i></button>
                    </div>
                </div>
            </div>
            </form>
    </div>
</div>
<script>
    function subscribe()
    {
        var emailpattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var email = $('#txtemail').val();
        if(email != "")
        {
            if(!emailpattern.test(email))
            {
                alert("Invalid Email");
                return false;
            }
            else
            {
                $.ajax({
                    url: 'index.php?route=module/newsletters/news',
                    type: 'post',
                    data: 'email=' + $('#txtemail').val(),
                    dataType: 'json',


                    success: function(json) {
                        $('#content').parent().before('<div class="alert alert-success">' + json.message + ' <button type="button" class="close" data-dismiss="alert"><i class="icon-close"></i></button></div>');

                    }

                });
                return false;
            }
        }
        else
        {
            alert("Email Is Require");
            $(email).focus();
            return false;
        }


    }
</script>

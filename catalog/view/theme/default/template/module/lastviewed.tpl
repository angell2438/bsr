<div class="container-item_module">
    <h3><?php echo $heading_title; ?></h3>
    <div class="row">
        <div class="wrapper_prod-slider">
            <?php foreach ($products as $product) { ?>
                <div class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="product-thumb transition">
                        <?php if ($product['news']) { ?>
                            <div class="plash new-prod">
                                <span>Новинка</span>
                            </div>
                        <?php } ?>
                        <?php if ($product['sales']) { ?>
                            <div class="plash sale-prod">
                                <span>Акція</span>
                            </div>
                        <?php } ?>
                        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                        <div class="caption">
                            <a class="name" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            <?php if($product['size_attribute']) { ?>
                                <div class="attributes">
                                    <div class="item option-name"><?php echo $product['size_attribute'][0]['name'] . ' ' . $product['size_attribute'][0]['text']; ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($product['options']) { ?>
                                <div class="option">
                                    <?php foreach ($product['options'] as $option) { ?>
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <div class="item">
                              <span class="option-name">
                                  <?php echo $option_value['name']; ?>
                              </span>
                                                <?php if ($option_value['full_price']) { ?>
                                                    <span class="price">
                                <?php if ($option_value['special_price']) { ?>
                                    <span class="price-old"><?php echo $option_value['full_price']; ?></span>
                                    <?php echo $option_value['special_price']; ?>
                                <?php } else { ?>
                                    <?php echo $option_value['full_price']; ?>
                                <?php } ?>
                                      </span>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="button-group">
                        <button class="mini-btn" type="button"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon-like-black"></i></button>
                        <button class="mini-btn" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="icon-basket"></i></button>
                        <button class="one_click" onclick="get_popup_purchase(<?php echo $product['product_id']; ?>);" data-dismiss="modal"><?php echo $button_purchase_now; ?></button>

                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

</div>

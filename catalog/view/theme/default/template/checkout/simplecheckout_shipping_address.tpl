<div class="simplecheckout-block" id="simplecheckout_payment_address" <?php echo $hide ? 'data-hide="true"' : '' ?> <?php echo $display_error && $has_error ? 'data-error="true"' : '' ?>>
    <?php if ($display_header) { ?>
        <div class="checkout-heading panel-heading"><?php echo 'dd' ?></div>
    <?php } ?>

    <?php foreach ($hidden_rows as $row) { ?>
      <?php echo $row ?>
    <?php } ?>
  </div>
</div>

<script type="text/javascript">
        $(document).ready(function(event) {


            var radio = $('input[name=shipping_method]:checked', '.radio').val();


            $('input[id=\'shipping_address_my_city\']').autocomplete({
                minLength: 2,
                delay: 500,
                source: function(request, response) {

                    var name = $('input[id=\'shipping_address_my_city\']').val();

                    if(name != ''){

                        $.ajax({
                            url: 'index.php?route=common/simple_connector&method=getCityd&custom=1&deliv='+ radio +'&name='+ name ,
                            dataType: 'json',
                            success: function(json) {
                                response($.map(json, function(item) {
                                    return {
                                        label: item.area + ', ' + item.city + ' (' + item.city_ru + ')',
                                        value: item.area + ', ' + item.city,
                                        my_id: item.city_id,
                                        area: item.area
                                    }
                                }));
                            }
                        });
                    }
                },
                select: function(item) {
                    $('input[id=\'shipping_address_my_city\']').val(item.value);
                    console.log(item);
                    //this.preventDefault();

                    return false;
                },

            });




        });

</script>

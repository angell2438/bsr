<?php echo $header; ?>
<div class="container">
          <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <?php if($i+1<count($breadcrumbs)) { ?>
              <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
              <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
        <div class="wrapper-margin-contact">
            <div class="row">
                <div class="col-md-6">
                    <div class="wrapper-contact-info">
                        <div class="row">
                            <div class="col-md-6">
<!--                                <div class="row">-->
                                    <div class="contact-info">
                                        <?php if ($comment) { ?>
                                            <?php echo $comment; ?>
                                        <?php } ?>
                                    </div>
<!--                                </div>-->
                            </div>
                            <div class="col-md-6">
<!--                                <div class="row">-->
                                    <div class="contact-info">
                                        <span class="icon-envelope"></span>
                                        <a class="bold-text" href="mailto:<?php echo $contact_email; ?>"><?php echo $contact_email; ?></a>
                                    </div>
<!--                                </div>-->
                            </div>
                            <?php if ($telephone) { ?>
                                <div class="col-md-6">
<!--                                    <div class="row">-->
                                        <div class="contact-info">
                                            <span class="icon-phone-call"></span>
                                            <a class="bold-text" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone ) ;?>"><span>Віталій</span>  <?php echo $telephone; ?></a>
                                        </div>
<!--                                    </div>-->
                                </div>
                            <?php } ?>
                            <?php if ($fax) { ?>
                                <div class="col-md-6">
<!--                                    <div class="row">-->
                                        <div class="contact-info">
                                            <span class="icon-phone-call"></span>
                                            <a class="bold-text" href="tel:<?php echo preg_replace("/[ ()-]/", "", $fax ) ;?>"><span>Олена</span>  <?php echo $fax; ?></a>
                                        </div>
<!--                                    </div>-->
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                            <div class="col-md-12">
                                <div class="form-group required">

                                    <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" placeholder="<?php echo $entry_name; ?>"/>
                                    <?php if ($error_name) { ?>
                                        <div class="text-danger"><?php echo $error_name; ?></div>
                                    <?php } ?>

                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group style-input required">
                                    <input type="text" name="phone" value="<?php echo $phone; ?>" id="input-phone" placeholder="Телефон" class="form-control" />
                                    <?php if ($error_phone) { ?>
                                        <div class="text-danger">Ви не ввели номер телефону!</div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group required">

                                    <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" placeholder="<?php echo $entry_email; ?>"/>
                                    <?php if ($error_email) { ?>
                                        <div class="text-danger"><?php echo $error_email; ?></div>
                                    <?php } ?>

                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group required">

                                    <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control" placeholder="<?php echo $entry_enquiry; ?>"><?php echo $enquiry; ?></textarea>
                                    <?php if ($error_enquiry) { ?>
                                        <div class="text-danger"><?php echo $error_enquiry; ?></div>
                                    <?php } ?>

                                </div>
                            </div>
                            <?php echo $captcha; ?>
                            <div class="buttons-top col-md-12">
                                <div class="pull-left">
                                    <input class="btn btn-pink" type="submit" value="<?php echo $btn_submit; ?>" />
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>


      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>

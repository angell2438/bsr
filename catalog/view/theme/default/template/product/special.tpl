<?php echo $header; ?>
<div class="container">
          <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <?php if($i+1<count($breadcrumbs)) { ?>
              <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
              <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9 col-sm-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
        <?php if ($products) { ?>
        <div class="sort-wrapper">
            <div class="row">
                <div class="flex-row">
                    <div class="col-md-3">
                        <div class="sort-text">
                            <?php echo $results; ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="flex-row limit-select">
                            <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                            <select id="input-limit" class="form-control" onchange="location = this.value;">
                                <?php foreach ($limits as $limits) { ?>
                                    <?php if ($limits['value'] == $limit) { ?>
                                        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="flex-row sort-select">
                            <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                            <select id="input-sort" class="form-control" onchange="location = this.value;">
                                <?php foreach ($sorts as $sorts) { ?>
                                    <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <?php foreach ($products as $product) { ?>
                <div class="product-layout product-list col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="product-thumb transition">
                        <?php if ($product['news']) { ?>
                            <div class="plash new-prod">
                                <span><?php echo $text_new; ?></span>
                            </div>
                        <?php } ?>
                        <?php if ($product['sales']) { ?>
                            <div class="plash sale-prod">
                                <span><?php echo $text_sale; ?></span>
                            </div>
                        <?php } ?>
                        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                        <div class="caption">
                            <a class="name" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            <?php if($product['size_attribute']) { ?>
                                <div class="attributes">
                                    <div class="item option-name"><?php echo $product['size_attribute'][0]['name'] . ' ' . $product['size_attribute'][0]['text']; ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($product['options']) { ?>
                                <div class="option">
                                    <?php foreach ($product['options'] as $option) { ?>
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <div class="item">
                              <span class="option-name">
                                  <?php echo $option_value['name']; ?>
                              </span>
                                                <?php if ($option_value['full_price']) { ?>
                                                    <span class="price">
                                <?php if ($option_value['special_price']) { ?>
                                    <span class="price-old"><?php echo $option_value['full_price']; ?></span>
                                    <?php echo $option_value['special_price']; ?>
                                <?php } else { ?>
                                    <?php echo $option_value['full_price']; ?>
                                <?php } ?>
                                      </span>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="button-group">
                        <button class="mini-btn" type="button"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon-like-black"></i></button>
                        <button class="mini-btn" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="icon-basket"></i></button>
                        <button class="one_click" onclick="get_popup_purchase(<?php echo $product['product_id']; ?>);" data-dismiss="modal"><?php echo $button_purchase_now; ?></button>

                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-sm-12 text-right"><?php echo $pagination; ?></div>
        </div>
        <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
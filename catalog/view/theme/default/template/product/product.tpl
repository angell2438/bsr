<?php echo $header; ?>
<div class="container" >

  <div class="row"><?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9 col-sm-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
        <ul class="breadcrumb"  itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <?php if($i+1<count($breadcrumbs)) { ?>
                        <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                    <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
        <?php echo $content_top; ?>
        <div class="wrapper-prod-card clearfix">
            <div class="">
                <?php if ($column_left || $column_right) { ?>
                    <?php $class = 'col-cart-img'; ?>
                <?php } else { ?>
                    <?php $class = 'col-sm-8'; ?>
                <?php } ?>
                <div class="<?php echo $class; ?>">
                    <?php if ($thumb || $images) { ?>
                        <div class="thumbnails">
                            <?php if ($thumb) { ?>
                                <a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                                    <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                                </a>
                            <?php } ?>
                            <?php if ($images) { ?>
                                <?php foreach ($images as $image) { ?>
                                    <a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>">
                                        <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                                    </a>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <?php if ($column_left || $column_right) { ?>
                    <?php $class = 'col-cart-info'; ?>
                <?php } else { ?>
                    <?php $class = 'col-sm-4'; ?>
                <?php } ?>
                <div class="<?php echo $class; ?>">
                    <div class="row">
                        <div class="wrapper-padd">
                            <h1><?php echo $heading_title; ?></h1>

                            <div id="product">
                                <?php if ($options) { ?>
                                    <?php foreach ($options as $option) { ?>
                                        <?php if ($option['type'] == 'select') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                                    <option value=""><?php echo $text_select; ?></option>
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'radio') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> margin-option">
                                                <div class="flex-row xs-flex-colum" id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio ">
                                                            <label class="form-checkbox input-group">
                                                                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />

                                                                <span class="form-checkbox__marker"></span>
                                                                <span class="form-checkbox__label option-name">
                                                        <?php echo $option_value['name']; ?>
                                                                    <?php if ($option_value['full_price']) { ?>
                                                                        <span class="price">
                                                                    <?php if ($option_value['special_price']) { ?>
                                                                        <span class="price-old"><?php echo $option_value['full_price']; ?></span>
                                                                        <?php echo $option_value['special_price']; ?>
                                                                    <?php } else { ?>
                                                                        <?php echo $option_value['full_price']; ?>
                                                                    <?php } ?>
                                                                </span>
                                                                    <?php } ?>
                                                        </span>
                                                            </label>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'checkbox') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                <?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </label>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'image') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </label>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'text') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                            </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'textarea') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                            </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'file') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                                <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                            </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'date') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <div class="input-group date">
                                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                    <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                                            </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'datetime') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <div class="input-group datetime">
                                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                            </div>
                                        <?php } ?>
                                        <?php if ($option['type'] == 'time') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <div class="input-group time">
                                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                                    <div class="prod-atrib">
                                        <?php if ($manufacturer) { ?>
                                                <div class="attributes">
                                                    <div class="item option-name"><span><?php echo $text_manufacturer; ?></span> <?php echo $manufacturer; ?></div>
                                                </div>
                                        <?php } ?>
                                    </div>
                                <?php if ($attribute_groups) { ?>
                                    <div class="prod-atrib">
                                        <?php foreach ($attribute_groups as $attribute_group) { ?>
                                            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                <div class="attributes">
                                                    <div class="item option-name"><span><?php echo $attribute['name']; ?>:</span> <?php echo $attribute['text']; ?></div>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                                <div class="btn-group wrapper-wish">
                                    <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="icon-like-black"></i> <?php echo $text_reward; ?></button>
                                </div>

                                <?php if ($recurrings) { ?>
                                    <hr>
                                    <h3><?php echo $text_payment_recurring ?></h3>
                                    <div class="form-group required">
                                        <select name="recurring_id" class="form-control">
                                            <option value=""><?php echo $text_select; ?></option>
                                            <?php foreach ($recurrings as $recurring) { ?>
                                                <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="help-block" id="recurring-description"></div>
                                    </div>
                                <?php } ?>
                                <div class="form-group flex-row flex-colum-xs">
                                    <div class="qty_block flex-row">
                                        <div class="qty_control minus" onclick="if(parseInt($('#input-quantity').val()) > parseInt($('#input-quantity').attr('data-min'))){$('#input-quantity').val(parseInt($('#input-quantity').val()) - 1);}"><i class="icon-down-arrow"></i></div>
                                        <div class="wrapp_inputs">
                                            <input class="qty_input" type="text" name="quantity"  value="<?php echo $minimum; ?>" data-min="<?php echo $minimum; ?>" placeholder="<?php echo $minimum; ?>" size="2" id="input-quantity"/>
                                        </div>
                                        <div class="qty_control plus" onclick="$('#input-quantity').val(parseInt($('#input-quantity').val()) + 1);"><i class="icon-up-arrow"></i></div>
                                        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                                    </div>

                                    <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-pink_by"> <i class="icon-basket"></i><?php echo $button_cart; ?></button>
                                    <button class='btn btn-default short-btn' onclick="get_popup_purchase('<?php echo $product_id; ?>');"><span><?php echo $button_click_by; ?></span></button>

                                </div>
                                <?php if ($minimum > 1) { ?>
                                    <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                                <?php } ?>
                            </div>

                            <?php if ($review_status) { ?>
                                <div class="rating">
                                    <!-- AddThis Button BEGIN -->
                                    <div class="share_wrapp flex-row">
                                        <div class="text"><i class="icon-share"></i><?php echo $text_sharing; ?></div>
                                        <div class="icon_wrapp flex-row">

                                            <div class="share_item facebook  icon-facebook">
                                                <a href="" onClick="openWin2();">facebook</a>
                                            </div>
                                            <div class="share_item twitter icon-twitter">
                                                <a class=""
                                                   href="https://twitter.com/share"
                                                   data-size="large"
                                                   data-text="custom share text"
                                                   data-url="https://dev.twitter.com/web/tweet-button"
                                                   data-hashtags="example,demo"
                                                   data-via="twitterdev"
                                                   data-related="twitterapi,twitter"
                                                   target="_blank">Twitter
                                                </a>
                                            </div>
                                            <div class="share_item google icon-google-plus">
                                                <a href="https://plus.google.com/share?url=<?php echo $og_url; ?>" target="_blank"></a>
                                            </div>
                                        </div>

                                        <div class="hidden">
                                            <div class="g-plus" data-action="share"></div>
                                            <a href="https://twitter.com/share" class="twitter-share-button" data-lang="ru">Твитнуть</a>
                                        </div>

                                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];
                                                if(!d.getElementById(id))
                                                {js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
                                        </script>
                                        <script>
                                            $('.facebook a').click(function (e) {
                                                e.preventDefault();
                                            })
                                            function openWin2() {
                                                myWin=open("http://www.facebook.com/sharer.php?u=<?php echo $og_url; ?>","displayWindow","width=520,height=300,left=350,top=170,status=no,toolbar=no,menubar=no");
                                            }
                                        </script>
                                    </div>
                                    <!-- AddThis Button END -->
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="wrapper-description">
                        <h3>
                            <?php echo $tab_description; ?>
                        </h3>
                        <div class="txt-description">
                            <?php echo $description; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <?php if ($products) { ?>
        <div class="container-item_module mini-title">
          <h3><?php echo $text_related; ?></h3>
          <div class="row">
              <div class="wrapper_prod-slider">
                <?php foreach ($products as $product) { ?>
                    <div class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="product-thumb transition">
                            <?php if ($product['news']) { ?>
                                <div class="plash new-prod">
                                    <span><?php echo $text_new; ?></span>
                                </div>
                            <?php } ?>
                            <?php if ($product['sales']) { ?>
                                <div class="plash sale-prod">
                                    <span><?php echo $text_sale; ?></span>
                                </div>
                            <?php } ?>
                            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                            <div class="caption">
                                <a class="name" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                <?php if($product['size_attribute']) { ?>
                                    <div class="attributes">
                                        <div class="item option-name"><?php echo $product['size_attribute'][0]['name'] . ' ' . $product['size_attribute'][0]['text']; ?></div>
                                    </div>
                                <?php } ?>
                                <?php if ($product['options']) { ?>
                                    <div class="option">
                                        <?php foreach ($product['options'] as $option) { ?>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="item">
                                                      <span class="option-name">
                                                          <?php echo $option_value['name']; ?>
                                                      </span>
                                                    <?php if ($option_value['full_price']) { ?>
                                                        <span class="price">
                                                            <?php if ($option_value['special_price']) { ?>
                                                                <span class="price-old"><?php echo $option_value['full_price']; ?></span>
                                                                <?php echo $option_value['special_price']; ?>
                                                            <?php } else { ?>
                                                                <?php echo $option_value['full_price']; ?>
                                                            <?php } ?>
                                                        </span>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="button-group">
                            <button class="mini-btn" type="button"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon-like-black"></i></button>
                            <button class="mini-btn" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="icon-basket"></i></button>
                            <button class="one_click" onclick="get_popup_purchase(<?php echo $product['product_id']; ?>);" data-dismiss="modal"><?php echo $button_click_by; ?></button>

                        </div>
                </div>
                <?php } ?>
              </div>
          </div>
        </div>
      <?php } ?>

      <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
  </div>
</div>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
                    console.log('1');
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}
                // console.log('1');
				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                $('#cart > button').html('<i class="icon-basket"></i>КОШИК<span id="cart-total_count">' + json['total_count'] + '</span><span id="cart-total"> ' + json['total'] + '</span>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > .cart-container > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger">' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success">' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<?php echo $footer; ?>

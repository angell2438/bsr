<div class="modal fade autorization_modal" id="autorization_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><?php echo $text_autorization; ?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <div class="col-sm-6">
                    <div class="title"><?php echo $text_login; ?></div>
                    <div id="quick-login">
                        <div class="form-group required mail">
                            <input type="text" name="email" value=""  placeholder="<?php echo $entry_email; ?>" id="input-email_l" class="input-ico form-control" />
                        </div>
                        <div class="form-group required password">
                            <input type="password" name="password" value="" placeholder="<?php echo $entry_password; ?>" id="input-password_l" class="input-ico form-control" />
                        </div>

                        <div class="fl_login">
                            <div class="show_pass_ch">
                                <input type="checkbox" id="show_pass">
                                <label for="show_pass">
                                    <?php echo $text_show_pass; ?>
                                </label>
                            </div>
                            <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                        </div>
                        <button type="button" class="btn btn-block loginaccount"  data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_login ?></button>
                        <div class="social_block">
                            <div class="title"><?php echo $text_social; ?></div>
                            <div class="wrapp_btn">
                                <button class="soc_btn facebook" onclick="dix_facebook()">Facebook</button>
                                <button class="soc_btn google" onclick="dix_googlePop('/glogin', 'ds', 700, 500)">Google +</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="title"><?php echo $text_register; ?></div>
                    <div id="quick-register">
                        <div class="form-group required mail">
                            <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="input-ico form-control" />
                        </div>
                        <div class="form-group required name">
                            <input type="text" name="name" value="" placeholder="<?php echo $entry_name; ?>" id="input-name" class="input-ico form-control" />
                        </div>
                        <div class="form-group required name">
                            <input type="text" name="lastname" value="" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                        </div>
                        <div class="form-group required password">
                            <input type="password" name="password" value="" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                        </div>
                        <div class="fl_login">
                            <div class="show_pass_ch">
                                <input type="checkbox" id="show_pass_r">
                                <label for="show_pass_r">
                                    <?php echo $text_show_pass; ?>
                                </label>
                            </div>
                        </div>

                        <button type="button" class="btn btn-block createaccount" data-loading-text="<?php echo $text_loading; ?>" ><?php echo $button_continue; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    $(document).delegate('.quick_login', 'click', function(e) {
        $('#modal-login').modal('show');
    });
    $(document).delegate('.quick_register', 'click', function(e) {
        $('#modal-register').modal('show');
    });
    //--></script>
<script type="text/javascript"><!--
    $('#quick-register input').on('keydown', function(e) {
        if (e.keyCode == 13) {
            $('#quick-register .createaccount').trigger('click');
        }
    });
    $('#quick-register .createaccount').click(function() {
        $.ajax({
            url: 'index.php?route=common/quicksignup/register',
            type: 'post',
            data: $('#quick-register input[type=\'text\'], #quick-register input[type=\'password\'], #quick-register input[type=\'checkbox\']:checked'),
            dataType: 'json',
            beforeSend: function() {
                $('#quick-register .createaccount').button('loading');
                $('#quick-register .alert-danger').remove();
            },
            complete: function() {
                $('#quick-register .createaccount').button('reset');
            },
            success: function(json) {
                $('#quick-register .form-group').removeClass('has-error');

                if(json['islogged']){
                    window.location.href="index.php?route=account/account";
                }
                if (json['error_email']) {
                    $('#quick-register #input-email').parent().addClass('has-error');
                    $('#quick-register #input-email').focus();
                }
                if (json['error_name']) {
                    $('#quick-register #input-name').parent().addClass('has-error');
                    $('#quick-register #input-name').focus();
                }
                if (json['error_lastname']) {
                    $('#quick-register #input-lastname').parent().addClass('has-error');
                    $('#quick-register #input-lastname').focus();
                }
                if (json['error_password']) {
                    $('#quick-register #input-password').parent().addClass('has-error');
                    $('#quick-register #input-password').focus();
                }
                if (json['error']) {
                    $('#autorization_modal #quick-register').prepend('<div class="alert alert-danger">' + json['error'] + '</div>');
                }

                if (json['now_login']) {
                    //$('.quick-login').before('<li class="dropdown"><a href="<?php //echo $account; ?>//" title="<?php //echo $text_account; ?>//" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php //echo $text_account; ?>//</span> <span class="caret"></span></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="<?php //echo $account; ?>//"><?php //echo $text_account; ?>//</a></li><li><a href="<?php //echo $order; ?>//"><?php //echo $text_order; ?>//</a></li><li><a href="<?php //echo $transaction; ?>//"><?php //echo $text_transaction; ?>//</a></li><li><a href="<?php //echo $download; ?>//"><?php //echo $text_download; ?>//</a></li><li><a href="<?php //echo $logout; ?>//"><?php //echo $text_logout; ?>//</a></li></ul></li>');
                    //
                    //$('.quick-login').remove();

                    window.location.href="index.php?route=account/account";

                }
                if (json['success']) {
                    $('#modal-register .main-heading').html(json['heading_title']);
                    success = json['text_message'];
                    success += '<div class="buttons"><div class="text-right"><a onclick="loacation();" class="btn btn-primary">'+ json['button_continue'] +'</a></div></div>';
                    $('#modal-register .modal-body').html(success);
                }
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('#quick-login input').on('keydown', function(e) {
        if (e.keyCode == 13) {
            $('#quick-login .loginaccount').trigger('click');
        }
    });
    $('#quick-login .loginaccount').click(function() {
        $.ajax({
            url: 'index.php?route=common/quicksignup/login',
            type: 'post',
            data: $('#quick-login input[type=\'text\'], #quick-login input[type=\'password\']'),
            dataType: 'json',
            beforeSend: function() {
                $('#quick-login .loginaccount').button('loading');
                $('#quick-login .alert-danger').remove();
            },
            complete: function() {
                $('#quick-login .loginaccount').button('reset');
            },
            success: function(json) {
                $('#modal-login .form-group').removeClass('has-error');
                if(json['islogged']){
                    window.location.href="index.php?route=account/account";
                }

                if (json['error']) {
                    $('#autorization_modal #quick-login').prepend('<div class="alert alert-danger">' + json['error'] + '</div>');

                    $('#quick-login input').parent().addClass('has-error');

                    $('#quick-login #input-email_l').focus();
                }
                if(json['success']){
                    loacation();
                    $('#autorization_modal').modal('hide');
                }

            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    function loacation() {
        location.reload();
    }
    //--></script>
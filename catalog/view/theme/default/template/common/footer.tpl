</div>
<?php echo $newsletters; ?>
<footer>
    <div class="menu-footer">
        <div class="container">
            <div class="flex-row">
                <div class="col-md-2 col-sm-4">
                    <div class="row">
                        <div class="logo">
                            <?php if ($logo) { ?>
                                <?php if ($home == $og_url) { ?>
                                    <img src="image/catalog/demo/artboard@2x.png" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                                <?php } else { ?>
                                    <a href="<?php echo $home; ?>">
                                        <img src="image/catalog/demo/artboard@2x.png" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                                    </a>
                                <?php } ?>
                            <?php } else { ?>
                                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 hidden-sm hidden-xs">
                    <div class="row">
                        <?php if ($categories) { ?>
                            <nav class="navbar menu-block">
                                <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
                                    <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
                                </div>
                                <div class="collapse navbar-collapse navbar-ex1-collapse">
                                    <ul class="nav navbar-nav menu-style">
                                        <li>
                                            <?php if ($home == $og_url) { ?>
                                                <p><?php echo $text_home; ?></p>
                                            <?php } else { ?>
                                                <a href="<?php echo $home; ?>"><?php echo $text_home; ?></a>
                                            <?php } ?>
                                        </li>
                                        <?php $k=''; foreach ($informations as $information) { $k++; ?>
                                            <?php if ($k == 1) { ?>
                                                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                                            <?php } ?>
                                            <?php if ($k == 2) { ?>
                                                <?php foreach ($categories as $category) { ?>
                                                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                                <?php } ?>
                                                <li><a href="<?php echo $sale; ?>"><?php echo $text_sale; ?></a></li>
                                                <li><a href="<?php echo $gallery; ?>"><?php echo $text_gallery; ?></a></li>
                                                <li><a href="<?php echo $download; ?>"><?php echo $text_download_page; ?></a></li>
                                                <?php foreach ($ncategories as $ncategory) { ?>
                                                    <li><a href="<?php echo $ncategory['href']; ?>"><?php echo $ncategory['name']; ?></a></li>
                                                <?php } ?>
                                                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                                                <li><a href="<?php echo $contact; ?>"><?php echo $contact_name; ?></a></li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </nav>

                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-2  col-sm-4">
                    <div class="row">
                        <ul class="social list-unstyled">
                            <?php if($config_fb){ ?>
                                <li class="social__item">
                                    <a class="social__link icon-facebook" href="<?php echo $config_fb; ?>" target="_blank"></a>
                                </li>
                            <?php } ?>
                            <?php if($config_vk){ ?>
                                <li class="social__item">
                                    <a class="social__link icon-vk" href="<?php echo $config_vk; ?>" target="_blank"></a>
                                </li>
                            <?php } ?>
                            <?php if($config_inst){ ?>
                                <li class="social__item">
                                    <a class="social__link icon-instagram" href="<?php echo $config_inst; ?>" target="_blank"></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hidden-lg hidden-md menu-tablet">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 hidden-lg hidden-md">
                    <div class="row">
                        <?php if ($categories) { ?>
                            <nav class="navbar menu-block">
<!--                                <div class="collapse navbar-collapse navbar-ex1-collapse">-->
                                    <ul class="nav navbar-nav menu-style">
                                        <li>
                                            <?php if ($home == $og_url) { ?>
                                                <p><?php echo $text_home; ?></p>
                                            <?php } else { ?>
                                                <a href="<?php echo $home; ?>"><?php echo $text_home; ?></a>
                                            <?php } ?>
                                        </li>
                                        <?php $k=''; foreach ($informations as $information) { $k++; ?>
                                            <?php if ($k == 1) { ?>
                                                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                                            <?php } ?>
                                            <?php if ($k == 2) { ?>
                                                <?php foreach ($categories as $category) { ?>
                                                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                                <?php } ?>
                                                <li><a href="<?php echo $sale; ?>"><?php echo $text_sale; ?></a></li>
                                                <li><a href="<?php echo $gallery; ?>"><?php echo $text_gallery; ?></a></li>
                                                <li><a href="<?php echo $download; ?>"><?php echo $text_download_page; ?></a></li>
                                                <?php foreach ($ncategories as $ncategory) { ?>
                                                    <li><a href="<?php echo $ncategory['href']; ?>"><?php echo $ncategory['name']; ?></a></li>
                                                <?php } ?>
                                                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                                                <li><a href="<?php echo $contact; ?>"><?php echo $contact_name; ?></a></li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
<!--                                </div>-->
                            </nav>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="copiright">
        <div class="container">
            <div class="row">
                <div class="flex-row xs-flex-colum">
                    <div class="col-md-10">
                        <p><?php echo $powered; ?></p>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-call" type="button" data-toggle="modal" data-target="#request_call-modal"><?php echo $text_contact; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<?php echo $quicksignup; ?>
<script src="catalog/view/javascript/jquery/slick/slick.js"></script>
<script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js"></script>
<script src="catalog/view/javascript/jquery/select2.min.js"></script>
<script src="catalog/view/javascript/jquery/machheigh.js"></script>
<script src="catalog/view/javascript/jquery/dotdotdot.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lightgallery.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-fullscreen.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-thumbnail.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-video.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-autoplay.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-zoom.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-hash.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-pager.js"></script>
<script src="catalog/view/javascript/main.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALGbIxt_aCHqxFzSPX-KopFPL5BvkRsHg&extension=.js"></script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
</body>
</html>
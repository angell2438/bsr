
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-close"></i></button>

        <div class="modal-content" id="quick-login">
			<div class="modal-header">
                <h3 class="modal-title"><?php echo $text_returning ?></h3>
			</div>
			<div class="modal-body">
				<div class="form-group required">
					<input type="text" name="email" value=""  id="input-email1" class="form-control" placeholder="<?php echo $entry_email; ?>"/>
				</div>
				<div class="form-group required">
					<input type="password" name="password" value="" id="input-password1" class="form-control" placeholder="<?php echo $entry_password; ?>"/>
				</div>
                <div class="fl_login">
                    <div class="show_pass_ch radio">
                        <label class="form-checkbox price-position" for="show_pass">
                            <input type="checkbox" id="show_pass"/>
                            <span class="form-checkbox__marker"></span>
                            <span class="form-checkbox__label"><?php echo $text_show_pass; ?></span>
                        </label>
                    </div>

                    <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                </div>
			</div>
            <button type="button" class="btn btn-primary loginaccount"  data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_login ?></button>
            <div class="social_block">
                <div class="title"><?php echo $text_social_login; ?></div>
                <div class="wrapp_btn">
                    <button class="soc_btn facebook" onclick="dix_facebook()"><span class="icon-facebook"></span></button>
                    <button class="soc_btn google" onclick="dix_googlePop('/glogin', 'ds', 700, 500)"><span class="icon-google"></span></button>
                </div>
            </div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-register" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-close"></i></button>
        <div class="modal-content" id="quick-register">
			<div class="modal-header">
				<h3 class="modal-title"><?php echo $text_new_customer ?></h3>
			</div>
			<div class="modal-body">
                <div class="form-group required">
                    <input type="text" name="email" value="" id="input-email" class="form-control"  placeholder="<?php echo $entry_email; ?>"/>
                </div>
				<div class="form-group required">
					<input type="text" name="name" value="" id="input-name1" class="form-control" placeholder="<?php echo $entry_name; ?>"/>
				</div>
                <div class="form-group required">
                    <input type="text" name="lastname" value="" id="input-lastname1" class="form-control" placeholder="<?php echo $entry_lastname; ?>"/>
                </div>
				<div class="form-group required">
					<input type="password" name="password" value="" id="input-password" class="form-control" placeholder="<?php echo $entry_password; ?>"/>
				</div>
                <div class="fl_login">
                    <div class="show_pass_ch radio">
                        <label class="form-checkbox price-position" for="show_pass_r">
                            <input type="checkbox" id="show_pass_r"/>
                            <span class="form-checkbox__marker"></span>
                            <span class="form-checkbox__label"><?php echo $text_show_pass; ?></span>
                        </label>
                    </div>
                </div>
                <?php if ($text_agree) { ?>
                    <input type="checkbox" name="agree" value="1" />&nbsp;<?php echo $text_agree; ?><br><br>
                    <button type="button" class="btn btn-primary createaccount"  data-loading-text="<?php echo $text_register; ?>" ><?php echo $text_register; ?></button>
                <?php }else{ ?>
                <button type="button" class="btn btn-primary createaccount" data-loading-text="<?php echo $text_register; ?>" ><?php echo $text_register; ?></button>
            </div>
            <?php } ?>
			</div>



		</div>
	</div>
</div>

<script type="text/javascript"><!--
$(document).delegate('.quick_login', 'click', function(e) {
	$('#modal-login').modal('show');
});
$(document).delegate('.quick_register', 'click', function(e) {
	$('#modal-register').modal('show');
});
//--></script>
<script type="text/javascript"><!--
$('#quick-register input').on('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#quick-register .createaccount').trigger('click');
	}
});
$('#quick-register .createaccount').click(function() {
	$.ajax({
		url: 'index.php?route=common/quicksignup/register',
		type: 'post',
		data: $('#quick-register input[type=\'text\'], #quick-register input[type=\'password\'], #quick-register input[type=\'checkbox\']:checked'),
		dataType: 'json',
		beforeSend: function() {
			$('#quick-register .createaccount').button('loading');
			$('#modal-register .alert-danger').remove();
		},
		complete: function() {
			$('#quick-register .createaccount').button('reset');
		},
		success: function(json) {
			$('#modal-register .form-group').removeClass('has-error');
			
			if(json['islogged']){
				 window.location.href="index.php?route=account/account";
			}
			if (json['error_name']) {
				$('#quick-register #input-name1').parent().addClass('has-error');
				$('#quick-register #input-name1').focus();
			}
			if (json['error_email']) {
				$('#quick-register #input-email').parent().addClass('has-error');
				$('#quick-register #input-email').focus();
			}
			if (json['error_telephone']) {
				$('#quick-register #input-telephone').parent().addClass('has-error');
				$('#quick-register #input-telephone').focus();
			}
			if (json['error_password']) {
				$('#quick-register #input-password').parent().addClass('has-error');
				$('#quick-register #input-password').focus();
			}
			if (json['error']) {
				$('#modal-register .modal-header').after('<div class="alert alert-danger" style="margin:5px;"> ' + json['error'] + '</div>');
			}
			
			if (json['now_login']) {
				$('.quick-login').before('<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li><li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li><li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li></ul></li>');
				
				$('.quick-login').remove();
			}
			if (json['success']) {
				$('#modal-register .main-heading').html(json['heading_title']);
				success = json['text_message'];
				success += '<div class="buttons"><div class="text-right"><a onclick="loacation();" class="btn btn-primary">'+ json['button_continue'] +'</a></div></div>';
				$('#modal-register .modal-body').html(success);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#quick-login input').on('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#quick-login .loginaccount').trigger('click');
	}
});
$('#quick-login .loginaccount').click(function() {
	$.ajax({
		url: 'index.php?route=common/quicksignup/login',
		type: 'post',
		data: $('#quick-login input[type=\'text\'], #quick-login input[type=\'password\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#quick-login .loginaccount').button('loading');
			$('#modal-login .alert-danger').remove();
		},
		complete: function() {
			$('#quick-login .loginaccount').button('reset');
		},
		success: function(json) {
			$('#modal-login .form-group').removeClass('has-error');
			if(json['islogged']){
				 window.location.href="index.php?route=account/account";
			}
			
			if (json['error']) {
				$('#modal-login .modal-header').after('<div class="alert alert-danger" style="margin:5px;"> ' + json['error'] + '</div>');
				$('#quick-login #input-email').parent().addClass('has-error');
				$('#quick-login #input-password').parent().addClass('has-error');
				$('#quick-login #input-email').focus();
			}
			if(json['success']){
				loacation();
				$('#modal-login').modal('hide');
			}
			
		}
	});
});
//--></script>
<script type="text/javascript"><!--
function loacation() {
	location.reload();
}
//--></script>
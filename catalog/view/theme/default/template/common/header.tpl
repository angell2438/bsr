<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="catalog/view/javascript/jquery/slick/slick-theme.css" rel="stylesheet">
    <link href="catalog/view/javascript/jquery/slick/slick.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/select2.min.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/lightgallery.css" rel="stylesheet">
<!--<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">-->
<link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<div class="container-for-menu">
    <button type="button" class="btn btn-toggle-menu close-menu">
        <i class="icon-close"></i>
    </button>
</div>
<div class="container-catalog_menu">
    <button type="button" class="btn btn-toggle-catalog close-menu">
        <i class="icon-close"></i>
    </button>
</div>
<nav class="top-info">
  <div class="container">
      <div class="wraper-shop-info">
              <?php if ($contact_email) {?>
                <a href="mailto:<?php echo $contact_email; ?>"><?php echo $contact_email; ?> <span class="hidden-xs">/</span></a>
              <?php } ?>
            <?php if ($telephone) {?>
                <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone ) ;?>">Віталій  <?php echo $telephone; ?> <span class="hidden-xs">/</span></a>
            <?php } ?>
              <?php if ($fax) {?>
                <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $fax ) ;?>">Олена  <?php echo $fax; ?></a>
              <?php } ?>
      </div>
  </div>
</nav>
<div class="page-wrapper panel-overlay">
<header>
  <div class="container">
    <div class="flex-row flex-colum-xs">
        <div class="sm-menu">
            <button type="button" class="btn btn-toggle-menu">
                <i class="icon-menu"></i>
            </button>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class="row">
                <div class="md-search"></div>

                <?php echo $search; ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-10 col-xs-offset-1">
          <div class="row">
              <div class="logo">
                  <?php if ($logo) { ?>
                      <?php if ($home == $og_url) { ?>
                          <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                      <?php } else { ?>
                          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
                      <?php } ?>
                  <?php } else { ?>
                      <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                  <?php } ?>
              </div>
          </div>
      </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="row">
                <div class="flex-container padding_login">
                    <div class="login-wrapper">
                        <?php if ($logged): ?>
                            <div class="items-help_elements">
                                <i class="icon-profile"></i>
                                <div class="items-container-flex_col xs-flex-row">
                                    <a class="login" href="<?= $account ?>"><?php echo $text_account; ?></a>
                                    <a class="register" href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="items-help_elements">
                                <i class="icon-profile"></i>
                                <div class="items-container-flex_col xs-flex-row">
                                    <a class="login" href="" data-toggle="modal" data-target="#modal-login"><?php echo $text_login; ?></a>
                                    <a class="register" href="" data-toggle="modal" data-target="#modal-register"><?php echo $text_register; ?></a>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php echo $language; ?>
                </div>
            </div>
      </div>
    </div>
      <div class="sm-search"></div>
  </div>
</header>
    <div class="cart_wish">
        <div class="wishlist_wrapp<?php echo ($text_wishlist > 0)? ' has_product':''?>">
            <a href="<?php echo $wishlist; ?>">
                <i class="icon-like-black"></i>
                <span class="count_item">
                    <span class="wish-text"><?php echo $text_wishlist; ?></span>
                </span>
            </a>
        </div>
        <?php echo $cart; ?>
    </div>

<?php if ($categories) { ?>
  <nav class="navbar menu-block">
      <div class="container">
          <div class="menu-catalog">
              <button type="button" class="btn btn-toggle-catalog">
                  <i class="icon-catalog"></i>
                  <span><?php echo $text_category; ?></span>
                  <i class="icon-up-arrow"></i>
              </button>
          </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse ex1-collapse-con">
          <ul class="nav navbar-nav menu-style mini-menu">
              <li>
                  <?php if ($home == $og_url) { ?>
                      <p><?php echo $text_home; ?></p>
                  <?php } else { ?>
                      <a href="<?php echo $home; ?>"><?php echo $text_home; ?></a>
                  <?php } ?>
              </li>
                  <?php $k=''; foreach ($informations as $information) { $k++; ?>
                      <?php if ($k == 1) { ?>
                        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                      <?php } ?>
                      <?php if ($k == 2) { ?>
                          <?php foreach ($categories as $category) { ?>
                              <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                          <?php } ?>
                          <li><a href="<?php echo $sale; ?>"><?php echo $text_sale; ?></a></li>
                          <li><a href="<?php echo $gallery; ?>"><?php echo $text_gallery; ?></a></li>
                          <li><a href="<?php echo $download; ?>"><?php echo $text_download_page; ?></a></li>
                          <?php foreach ($ncategories as $ncategory) { ?>
                              <li><a href="<?php echo $ncategory['href']; ?>"><?php echo $ncategory['name']; ?></a></li>
                          <?php } ?>
                          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                          <li><a href="<?php echo $contact; ?>"><?php echo $contact_name; ?></a></li>
                      <?php } ?>
                  <?php } ?>
          </ul>
        </div>
      </div>
  </nav>

<?php } ?>
    <div class="main-content">

<div id="cart" class="wrapper-cart">
  <button type="button" data-toggle="dropdown" data-loading-text="<?php echo $text_loading; ?>"
          class="btn btn-inverse btn-left-cart dropdown-toggle"><i class="icon-basket"></i>
      <span>КОШИК</span>
      <span id="cart-total_count"><?php echo $text_items_count; ?></span>
      <span id="cart-total"><?php echo $text_items; ?></span>
  </button>
    <div class="cart-container">
            <?php if ($products || $vouchers) { ?>
                <ul>
                    <li>
                        <div class="items">
                            <?php foreach ($products as $product) { ?>
                                <div class="item-cart">
                                    <div class="text-center"><?php if ($product['thumb']) { ?>
                                            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                                        <?php } ?></div>
                                    <div class="info-cart">
                                        <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                        <div class="cart-atributs">
                                            <?php if ($product['option']) { ?>
                                                <?php foreach ($product['option'] as $option) { ?>
                                                    - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php if ($product['recurring']) { ?>
                                                - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
                                            <?php } ?>
                                        </div>
                                       <div class="cart-price">
                                           <?php echo $product['total']; ?>
                                       </div>
                                    </div>

                                    <button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="close"><i class="icon-close"></i></button>
                                </div>
                            <?php } ?>
                            <?php foreach ($vouchers as $voucher) { ?>
                                <tr>
                                    <td class="text-center"></td>
                                    <td class="text-left"><?php echo $voucher['description']; ?></td>
                                    <td class="text-right">x&nbsp;1</td>
                                    <td class="text-right"><?php echo $voucher['amount']; ?></td>
                                    <td class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
                                </tr>
                            <?php } ?>
                        </div>
                    </li>
                    <li class="footer-cart">
                        <div>
                            <?php foreach ($totals as $total) { ?>
                                <div class="total-block">
                                   <p class="text-total"><?php echo $total['title']; ?></p>
                                   <p class="total-sum"><?php echo $total['text']; ?></p>
                                </div>
                            <?php } ?>
                            <a class="btn btn-pink" href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a>
                        </div>
                    </li>
                </ul>

            <?php } else { ?>
                <ul>
                    <li>
                        <p class="text-center"><?php echo $text_empty; ?></p>
                    </li>
                </ul>
            <?php } ?>

    </div>

</div>

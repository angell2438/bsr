<?php if (!$ajax && !$popup && !$as_module) { ?>
<?php $simple_page = 'simpleedit'; include $simple_header; ?>
<div class="simple-content">
<?php } ?>
    <?php if (!$ajax || ($ajax && $popup)) { ?>
    <script type="text/javascript">
    (function($) {
    <?php if (!$popup && !$ajax) { ?>
        $(function(){
    <?php } ?>
            if (typeof Simplepage === "function") {
                var simplepage = new Simplepage({
                    additionalParams: "<?php echo $additional_params ?>",
                    additionalPath: "<?php echo $additional_path ?>",
                    mainUrl: "<?php echo $action; ?>",
                    mainContainer: "#simplepage_form",
                    scrollToError: <?php echo $scroll_to_error ? 1 : 0 ?>,
                    javascriptCallback: function() {<?php echo $javascript_callback ?>}
                });

                simplepage.init();
            }
    <?php if (!$popup && !$ajax) { ?>
        });
    <?php } ?>
    })(jQuery || $);
    </script>
    <?php } ?>
    <div class="row">
        <div class="col-md-3">
            <div class="account_title">
                <?php echo $text_account; ?>
            </div>
            <ul class="account_left_bar">
                <li class="active"><a href="<?php echo $link_profil; ?>"><?php echo $text_profil; ?></a></li>
                <li><a href="<?php echo $link_password; ?>"><?php echo $text_password; ?></a></li>
                <li><a href="<?php echo $link_history; ?>"><?php echo $text_history; ?></a></li>
                <li><a href="<?php echo $link_logout; ?>"><?php echo $text_logout; ?></a></li>
            </ul>
        </div>
        <div class="col-md-9 col-sm-12">
            <div class="account_title">
                <?php echo $text_profil; ?>
            </div>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="simplepage_form">
                <div class="simpleregister" id="simpleedit">
                    <?php if ($error_warning) { ?>
                        <div class="warning alert alert-danger"><?php echo $error_warning; ?></div>
                    <?php } ?>
                    <div class="simpleregister-block-content">
                        <?php foreach ($rows as $row) { ?>
                            <?php echo $row ?>
                        <?php } ?>
                    </div>
                    <div class="simpleregister-button-block buttons">
                        <div class="simpleregister-button-left">
                            <a class="button btn btn-primary" data-onclick="submit" id="simpleregister_button_confirm"><span><?php echo $button_save; ?></span></a>
                        </div>
                    </div>
                </div>
                <?php if ($redirect) { ?>
                    <script type="text/javascript">
                        location = "<?php echo $redirect ?>";
                    </script>
                <?php } ?>
            </form>
        </div>
    </div>
<?php if (!$ajax && !$popup && !$as_module) { ?>
</div>
<?php include $simple_footer ?>
<?php } ?>
<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <?php if($i+1<count($breadcrumbs)) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><span><?php echo $breadcrumb['text']; ?></span></a></li>
            <?php }else{ ?>
                <li><span><?php echo $breadcrumb['text']; ?></span></li>
            <?php } ?>
        <?php } ?>
    </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
      <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
          <div class="row">
              <div class="col-md-3">
                  <div class="account_title">
                      <?php echo $text_account; ?>
                  </div>
                  <ul class="account_left_bar">
                      <li><a href="<?php echo $link_profil; ?>"><?php echo $text_profil; ?></a></li>
                      <li><a href="<?php echo $link_password; ?>"><?php echo $text_password; ?></a></li>
                      <li class="active"><a href="<?php echo $link_history; ?>"><?php echo $text_history; ?></a></li>
                      <li><a href="<?php echo $link_logout; ?>"><?php echo $text_logout; ?></a></li>
                  </ul>
              </div>
              <div class="col-md-9 col-sm-12">
                  <div class="account_title">
                      <?php echo $heading_title; ?>
                  </div>
                  <?php if ($orders) { ?>
                      <div class="order_histor_list">
                          <?php foreach ($orders as $order) { ?>
                              <div class="order_histor_item">
                                  <div class="history_block">
                                      <div class="options">
                                          <div class="option_item"><?php echo $text_qty; ?>: <?php echo $order['order_quantity']; ?></div>
                                      </div>
                                      <div class="price"><?php echo $order['total']; ?></div>
                                  </div>
                                  <div class="history_block">
                                      <div class="text_status"><?php echo $column_status; ?></div>
                                      <div class="status"><?php echo $order['status']; ?></div>
                                      <div class="date"><?php echo $order['date_added']; ?></div>
                                  </div>
                                  <div class="history_block">
                                      <div class="inside">
                                          <a href="<?php echo $order['href']; ?>"><?php echo $button_view; ?></a>
                                      </div>
                                  </div>
                              </div>
                          <?php } ?>
                      </div>
                  <?php }else{ ?>
                      <p><?php echo $text_empty; ?></p>
                  <?php } ?>
                  <br>
                  <br>
                  <div class="buttons clearfix">
                      <div class="pull-left"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                  </div>
              </div>
          </div>
          <?php echo $content_bottom; ?></div>

    <?php echo $column_right; ?></div>
    <br>
</div>
<?php echo $footer; ?>
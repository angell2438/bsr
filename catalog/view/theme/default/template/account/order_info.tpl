<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <?php if($i+1<count($breadcrumbs)) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><span><?php echo $breadcrumb['text']; ?></span></a></li>
            <?php }else{ ?>
                <li><span><?php echo $breadcrumb['text']; ?></span></li>
            <?php } ?>
        <?php } ?>
    </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
      <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

          <div class="row">
              <div class="col-md-3">
                  <div class="account_title">
                      <?php echo $text_account; ?>
                  </div>
                  <ul class="account_left_bar">
                      <li><a href="<?php echo $link_profil; ?>"><?php echo $text_profil; ?></a></li>
                      <li><a href="<?php echo $link_password; ?>"><?php echo $text_password; ?></a></li>
                      <li class="active"><a href="<?php echo $link_history; ?>"><?php echo $text_history; ?></a></li>
                      <li><a href="<?php echo $link_logout; ?>"><?php echo $text_logout; ?></a></li>
                  </ul>
              </div>
              <div class="col-md-9 col-sm-12">
                  <div class="account_title">
                      <?php echo $heading_title; ?>
                  </div>
                  <div class="order_wrapp">
                      <div class="top_panel">
                          <div class="status"><?php echo $column_status; ?>: <span><?php echo $last_ordr_status; ?></span></div>
                          <div class="date"><?php echo $date_added; ?></div>
                      </div>
                      <div class="products">
                          <?php foreach ($products as $product) { ?>
                              <div class="pr_item">
                                  <div class="wrapp_img">
                                      <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['image']; ?>" class="img-responsive" alt="<?php echo $product['name']; ?>"></a>
                                  </div>
                                  <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                                  <?php if($product['sku']) { ?>
                                      <div class="article"><?php echo $text_code; ?> <?php echo $product['sku']; ?></div>
                                  <?php } ?>
                                  <div class="options">
                                      <?php if($product['option']){ ?>
                                          <?php foreach($product['option'] as $option){ ?>
                                              <div class="option_item"><?php echo $option['name']; ?>: <?php echo $option['value']; ?></div>
                                          <?php } ?>
                                      <?php } ?>
                                      <div class="option_item"><?php echo $text_qty; ?> <?php echo $product['quantity']; ?></div>
                                  </div>
                                  <div class="price">
                                      <div><?php echo $product['price']; ?></div>
                                      <div><?php echo $product['total']; ?></div>
                                  </div>
                              </div>
                          <?php } ?>
                      </div>
                      <div class="total">
                          <?php foreach ($totals as $total) { ?>
                              <div class="total_item">
                                  <div class="title"><?php echo $total['title']; ?>:</div>
                                  <div class="text"><?php echo $total['text']; ?></div>
                              </div>
                          <?php } ?>
                      </div>
                      <div class="customer_info">
                          <?php if ($customer_info['payment_method']) { ?>
                              <div class="customer_item">
                                  <div class="text1"><?php echo $text_pay; ?></div>
                                  <div class="text2"><?php echo $customer_info['payment_method']; ?></div>
                              </div>
                          <?php } ?>
                          <?php if ($customer_info['shipping_method']) { ?>
                              <div class="customer_item">
                                  <div class="text1"><?php echo $text_shipp; ?></div>
                                  <div class="text2"><?php echo $customer_info['shipping_method']; ?></div>
                              </div>
                          <?php } ?>
                          <div class="customer_item">

                              <div class="text1"><?php echo $text_fln; ?></div>
                              <div class="text2"><?php echo $customer_info['firstname']; ?> <?php echo $customer_info['lastname']; ?></div>
                          </div>
                          <?php if ($customer_info['telephone']) { ?>
                              <div class="customer_item">
                                  <div class="text1"><?php echo $text_tel; ?></div>
                                  <div class="text2"><?php echo $customer_info['telephone']; ?></div>
                              </div>
                          <?php } ?>
                          <?php if ($customer_info['shipping_address_1']) { ?>
                              <div class="customer_item">
                                  <div class="text1"><?php echo $text_shipp_address; ?></div>
                                  <div class="text2"><?php echo $customer_info['shipping_address_1']; ?></div>
                              </div>
                          <?php } ?>
                          <?php if (strstr($customer_info['comment'], 'Адрес:')) { ?>
                              <div class="customer_item">
                                  <div class="text1"><?php echo $text_shipp_address; ?></div>
                                  <!--                                        нисколичко не костыль-->
                                  <div class="text2"><?php echo str_replace('Адрес:', '', strstr($customer_info['comment'], 'Адрес:')); ?></div>
                              </div>
                          <?php } ?>
                          <?php if ($customer_info['email']) { ?>
                              <div class="customer_item">
                                  <div class="text1"><?php echo $text_email; ?></div>
                                  <div class="text2"><?php echo $customer_info['email']; ?></div>
                              </div>
                          <?php } ?>
                      </div>
                  </div>
                  <br>
                  <br>
                  <div class="buttons clearfix">
                      <div class="pull-left"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                  </div>
              </div>
          </div>

          <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
    <br>
<?php echo $footer; ?>
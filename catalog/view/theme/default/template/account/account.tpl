<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <?php if($i+1<count($breadcrumbs)) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><span><?php echo $breadcrumb['text']; ?></span></a></li>
            <?php }else{ ?>
                <li><span><?php echo $breadcrumb['text']; ?></span></li>
            <?php } ?>
        <?php } ?>
    </ul>
    <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <div class="account_title">
                <?php echo $text_account; ?>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <ul class="account_left_bar">
                        <li><a href="<?php echo $link_profil; ?>"><?php echo $text_profil; ?></a></li>
                        <li><a href="<?php echo $link_password; ?>"><?php echo $text_password; ?></a></li>
                        <li><a href="<?php echo $link_history; ?>"><?php echo $text_history; ?></a></li>
                        <li><a href="<?php echo $link_logout; ?>"><?php echo $text_logout; ?></a></li>
                    </ul>
                </div>
            </div>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>

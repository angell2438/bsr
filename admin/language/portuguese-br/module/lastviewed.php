<?php
// Heading
$_['heading_title']    = 'Últimos Visitados';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Sucesso: você modificou o módulo "últimos visitados"!';
$_['text_edit']        = 'Editar o módulo Útimos Visitados';

// Entry
$_['entry_name']       = 'Nome do módulo';
$_['entry_limit']      = 'Limite';
$_['entry_width']      = 'Largura';
$_['entry_height']     = 'Altura';
$_['entry_status']     = 'Status';

// Error
$_['error_permission']  = 'Atenção: você não tem permissão para modificar este módulo!';
$_['error_width']       = 'Largura necessária!';
$_['error_height']      = 'Altura ncessária!';
<?php
class ControllerModuleGallery extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('module/gallery');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $this->model_setting_setting->editSetting('gallery', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_yes'] = $this->language->get('text_yes');
        $data['text_no'] = $this->language->get('text_no');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_title'] = $this->language->get('entry_title');
        $data['entry_banner'] = $this->language->get('entry_banner');
        $data['entry_resize'] = $this->language->get('entry_resize');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_thumb_title'] = $this->language->get('entry_thumb_title');

        $data['help_resize'] = $this->language->get('help_resize');
        $data['help_auto'] = $this->language->get('help_auto');
        $data['help_title'] = $this->language->get('help_title');
        $data['help_thumb_title'] = $this->language->get('help_thumb_title');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['action'] = $this->url->link('module/gallery', 'token=' . $this->session->data['token'], 'SSL');

        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');


        if (isset($this->request->post['gallery_id'])) {
            $data['filter_banner_id'] = $this->request->post['gallery_id'];
        }else {
            $data['filter_banner_id'] = $this->config->get('gallery_id');
        }

        $this->load->model('design/banner');

        $data['banners'] = $this->model_design_banner->getBanners();

        if (isset($this->request->post['gallery_status'])) {
            $data['status'] = $this->request->post['gallery_status'];
        }else{
            $data['status'] = $this->config->get('gallery_status');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/gallery.tpl', $data));
    }

    protected function validate()
    {
//        if (!$this->user->hasPermission('modify', 'module/banner')) {
//            $this->error['warning'] = $this->language->get('error_permission');
//        }
//
//        if ((utf8_strlen($this->request->post['config_galery_name']) < 3) || (utf8_strlen($this->request->post['config_galery_name']) > 64)) {
//            $this->error['name'] = $this->language->get('error_name');
//        }
//
//        if (!$this->request->post['config_galery_id']) {
//            $this->error['banner'] = $this->language->get('error_banner');
//        }

        return !$this->error;
    }
}